# HDst_WAN_Industries

Custom weapons for the gzDOOM mod Hideous Destructor.
Some weapons are based on Marathon.

Requires BulletLib Recasted
https://github.com/HDest-Community/HDBulletLib-Recasted

More weapons one day. Or year.

WSTE-M5 Combat Shotgun	: 'wst'

MA-75B Battle Rifle 	: 'm75'
- MA-75B clip '752'
- MA-75B grenade pack 'rk7'

MA-76C Assault Rifle	: 'm76'
- MA-76C clip '452'
- MA-76C 20mm grenade pack '2g7'
- 20mm grenade '2gr'

SPNKR-XP SSM Launcher	: 'spk'
- thunder rocket 'vrk'
- tortoise rocket 'trt'

MAUG-03B 'Norden'		: 'mu3'