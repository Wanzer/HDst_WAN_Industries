// using Peppergrinder's event handler code for the time being, i just *really* want be able to disable specific item spawns for now

AddOptionMenu "OptionsMenu"
{
	Submenu "$WANINDUSTRIES_MENU", "WANMenu"
}

OptionMenu "WANMenu"
{
	Title "WAN Industries Options"
	
	// [Ace] You can have up to 32 of these. On a single CVar, that is.
	// Would have been 64 if GZDoom integers were 8-bytes but oh well.
	StaticText "----- Weapon Spawns -----", "Teal"
	WanSpawnOption "WSTE-M5 Combat Shotgun", "wan_weaponspawns_1", "YesNo", 0
	WanSpawnOption "MA-75B Battle Rifle", "wan_weaponspawns_1", "YesNo", 1
	WanSpawnOption "MA-76C Assault Rifle", "wan_weaponspawns_1", "YesNo", 2
	WanSpawnOption "SPNKR-XP SSM Launcher", "wan_weaponspawns_1", "YesNo", 3
	Command "Enable All", "WAN_EnableAll_Weapons"
	Command "Disable All", "wan_weaponspawns_1 0"
	StaticText ""
	StaticText "----- Ammo Spawns -----", "Teal"
	WanSpawnOption "MA-75B Clips", "wan_ammospawns_1", "YesNo", 0
	WanSpawnOption "MA-75B Grenade Packs", "wan_ammospawns_1", "YesNo", 1
	WanSpawnOption "MA-76C Clips", "wan_ammospawns_1", "YesNo", 2
	WanSpawnOption "MA-76C 20mm Grenade Packs", "wan_ammospawns_1", "YesNo", 3
	WanSpawnOption "SPNKR-XP Custom Rockets", "wan_ammospawns_1", "YesNo", 4
	
	Command "Enable All", "WAN_EnableAll_Ammo"
	Command "Disable All", "wan_ammospawns_1 0"
}
