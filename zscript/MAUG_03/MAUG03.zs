// ------------------------------------------------------------
// MAUG-03 'Norden'
// ------------------------------------------------------------
// a tangled mess of ifs, else ifs, gotos, poor coding practices, and arbritrary decisions await you
const HDLD_MAUG03 = "mu3";

class WAN_MAUG03:HDWeapon{
	bool maug03open;
	default{
		weapon.slotnumber 4;
		weapon.slotpriority 1.3;
		weapon.kickback 20;
		weapon.selectionorder 26;
		inventory.pickupsound "misc/w_pkup";
		//inventory.pickupmessage "You got the MAUG-03B!";
		weapon.bobrangex 0.32;
		weapon.bobrangey 1.0;
		weapon.bobspeed 2.3;
		scale 0.7;
		obituary "$WAN_OBIT_MAUG03";
		hdweapon.refid HDLD_MAUG03;
		tag "$WAN_TAG_MAUG03";
		inventory.icon "MAU3Z0";
	}
	int counter;
	
	override string pickupmessage() {return Stringtable.Localize("$WAN_PICK_MAUG03");}
	
	override void tick(){
		super.tick();
		drainheat(MAUG3S_HEAT,24);
		// remove/comment out these lines starting from here
		let hdp=hdplayerpawn(owner);
		counter++;
		if(counter%3500==0){
			counter=0;
			if(!random(0,1)){
				if(hdp)itakeabite(hdp);//when inside a player inventory
				else munch();//when on the ground
			}
		}
		// and ending here, to exorcise the gremlin
	}
	// and remove this function if it ever breaks or causes issues
	action void itakeabite(actor who){
		if(who){
			let eatmag=hdmagammo(who.findinventory("HD4mMag"));
			if(eatmag){
				int mindex=eatmag.mags.size()-1;
				if(eatmag.mags.size()<1||eatmag.mags[mindex]<1){
					eatmag.TakeMag(false);
					HDMagAmmo.SpawnMag(who,"HD4mMag",0);
					who.A_StartSound("misc/wanfx01",8,CHANF_OVERLAP);
					return;
				}
				int totake=min(random(1,12),eatmag.mags[mindex]);
				eatmag.mags[mindex]-=totake;
				who.A_StartSound("misc/wanfx02",8,CHANF_OVERLAP);
			}else{
				switch (random(1,3)){
					case 1:
						if(invoker.weaponstatus[MAUG3S_MAG1]>0){
							int totake=min(random(1,12),invoker.weaponstatus[MAUG3S_MAG1]);
							invoker.weaponstatus[MAUG3S_MAG1]-=totake;
							who.A_StartSound("misc/wanfx02",8,CHANF_OVERLAP);
						}else who.A_StartSound("misc/wanfx01",8,CHANF_OVERLAP);
						break;
					case 2:
						if(invoker.weaponstatus[MAUG3S_MAG2]>0){
							int totake=min(random(1,12),invoker.weaponstatus[MAUG3S_MAG2]);
							invoker.weaponstatus[MAUG3S_MAG2]-=totake;
							who.A_StartSound("misc/wanfx02",8,CHANF_OVERLAP);
						}else who.A_StartSound("misc/wanfx01",8,CHANF_OVERLAP);
						break;
					case 3:
						if(invoker.weaponstatus[MAUG3S_MAG3]>0){
							int totake=min(random(1,12),invoker.weaponstatus[MAUG3S_MAG3]);
							invoker.weaponstatus[MAUG3S_MAG3]-=totake;
							who.A_StartSound("misc/wanfx02",8,CHANF_OVERLAP);
						}else who.A_StartSound("misc/wanfx01",8,CHANF_OVERLAP);
						break;
				}
			}
		}
	}
	//
	
	// and this one too
	action void munch(){
		int mag1;
		int mag2;
		int mag3;
		mag1=invoker.weaponstatus[MAUG3S_MAG1];
		mag2=invoker.weaponstatus[MAUG3S_MAG2];
		mag3=invoker.weaponstatus[MAUG3S_MAG3];
		switch (random(1,3)){
			case 1:
				if(mag1>0){
					int totake=min(random(1,12),invoker.weaponstatus[MAUG3S_MAG1]);
					invoker.weaponstatus[MAUG3S_MAG1]-=totake;
					A_StartSound("misc/wanfx02",8,CHANF_OVERLAP);
				}else A_StartSound("misc/wanfx01",8,CHANF_OVERLAP);
				break;
			case 2:
				if(mag2>0){
					int totake=min(random(1,12),invoker.weaponstatus[MAUG3S_MAG2]);
					invoker.weaponstatus[MAUG3S_MAG2]-=totake;
					A_StartSound("misc/wanfx02",8,CHANF_OVERLAP);
				}else A_StartSound("misc/wanfx01",8,CHANF_OVERLAP);
				break;
			case 3:
				if(mag3>0){
					int totake=min(random(1,12),invoker.weaponstatus[MAUG3S_MAG3]);
					invoker.weaponstatus[MAUG3S_MAG3]-=totake;
					A_StartSound("misc/wanfx02",8,CHANF_OVERLAP);
				}else A_StartSound("misc/wanfx01",8,CHANF_OVERLAP);
				break;
		}
		if(mag1<1&&mag2<1&&mag3<1){
			if(!random(0,10)){
				A_SpawnItemEx("WAN_MAUG03_BROKEN");
				Destroy();//perish
			}
		}
	}
	//
	
	// remove this override to remove the gremlin
	override void GunBounce(){
		super.GunBounce();
		if(!random(0,4)){
			A_StartSound("misc/wanfx01",8,CHANF_OVERLAP);
			if(!random(0,60)){
				A_SpawnItemEx("WAN_MAUG03_BROKEN");
				Destroy();//perish
			}
		}
	}
	//
	
	// remove this override to remove the gremlin
	override void OnPlayerDrop(){
		if(!random(0,4))A_StartSound("misc/wanfx01",8,CHANF_OVERLAP);
	}
	//
	
	override bool AddSpareWeapon(actor newowner){return AddSpareWeaponRegular(newowner);}
	
	override hdweapon GetSpareWeapon(actor newowner,bool reverse,bool doselect){return GetSpareWeaponRegular(newowner,reverse,doselect);}
	
	override void postbeginplay(){
		super.postbeginplay();
		barrelwidth=3;
		barreldepth=3;
		barrellength=31;
		bfitsinbackpack=false;
		
		weaponspecial=1337;
	}
	
	override void DropOneAmmo(int amt){
		if(owner){
			amt=clamp(amt,1,10);
			if(owner.countinv("FourMilAmmo"))owner.A_DropInventory("FourMilAmmo",50);
			else{
				owner.angle-=10;
				owner.A_DropInventory("HD4mMag",1);
				owner.angle+=20;
				owner.A_DropInventory("HDBattery",1);
				owner.angle-=10;
			}
		}
	}
	
	override void ForceBasicAmmo(){
		owner.A_TakeInventory("FourMilAmmo");
		owner.A_TakeInventory("HD4mMag");
		owner.A_GiveInventory("HD4mMag",3);
		owner.A_TakeInventory("HDBattery");
		owner.A_GiveInventory("HDBattery");
	}
	
	override double gunmass(){
		double mass=9+(weaponstatus[MAUG3S_BATTERY]<0?0:1);//slightly less mass than a vulc
		for(int i=MAUG3S_MAG1;i<=MAUG3S_MAG3;i++){
			if(weaponstatus[i]>=0){
				mass+=weaponstatus[i]*0.04;//~2 mass per full mag
			}
		}
		return mass;
	}
	
	override double weaponbulk(){
		double blx=185;//slightly awkward shape but weighs less than a vulc
		for(int i=MAUG3S_MAG1;i<=MAUG3S_MAG3;i++){
			int wsi=weaponstatus[i];
			if(wsi>=0)blx+=ENC_426_LOADED*wsi+ENC_426MAG_LOADED;
		}
		return blx;
	}
	
	override string,double getpickupsprite(){//placeholder
		string spr;

		//if(1<0)spr="B";
		//else spr="A";

		return "MAU3".."Z".."0",1.;
	}
	
	override void DrawHUDStuff(HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl){
		if(sb.hudlevel==1){
			int nextmagloaded=sb.GetNextLoadMag(hdmagammo(hpl.findinventory("HD4mMag")));
			if(nextmagloaded>50){
				sb.drawimage("ZMAGA0",(-46,-3),sb.DI_SCREEN_CENTER_BOTTOM,scale:(2,2));
			}else if(nextmagloaded<1){
				sb.drawimage("ZMAGC0",(-46,-3),sb.DI_SCREEN_CENTER_BOTTOM,alpha:nextmagloaded?0.6:1.,scale:(2,2));
			}else sb.drawbar(
				"ZMAGNORM","ZMAGGREY",
				nextmagloaded,50,
				(-46,-3),-1,
				sb.SHADER_VERT,sb.DI_SCREEN_CENTER_BOTTOM
			);
			sb.drawbattery(-64,-4,sb.DI_SCREEN_CENTER_BOTTOM,reloadorder:true);
			sb.drawnum(hpl.countinv("HD4mMag"),-43,-8,sb.DI_SCREEN_CENTER_BOTTOM);
			sb.drawnum(hpl.countinv("HDBattery"),-56,-8,sb.DI_SCREEN_CENTER_BOTTOM);
		}
		bool bat=hdw.weaponstatus[MAUG3S_BATTERY]>0;
		int lod=max(hdw.weaponstatus[MAUG3S_MAG1],0);
		int lod2=max(hdw.weaponstatus[MAUG3S_MAG2],0);
		int lod3=max(hdw.weaponstatus[MAUG3S_MAG3],0);
		color redCol = color(255,200,20,32);
		
		if(maug03open){
			int selected=hdw.weaponstatus[MAUG3S_SELECTED];
			switch (selected){
				case 1:
				case 2:
				case 3:
					sb.Fill(redCol,-19,-19+selected*3,4,1,sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_ITEM_RIGHT);
					sb.Fill(redCol,-19,-16+selected*3,4,1,sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_ITEM_RIGHT);
					sb.Fill(redCol,-16,-19+selected*3,1,4,sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_ITEM_RIGHT);
					break;
				case 4:
					sb.Fill(redCol,-19,-6,4,1,sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_ITEM_RIGHT);
					sb.Fill(redCol,-19,-3,4,1,sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_ITEM_RIGHT);
					sb.Fill(redCol,-16,-6,1,4,sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_ITEM_RIGHT);
					break;
			}
		}
		
		string llba="STBURAUT";
		if(hdw.weaponstatus[0]&LIBF_FULLAUTO)llba="STFULAUT";
		sb.drawimage(
			llba,(-15,-18),
			sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_TRANSLATABLE|sb.DI_ITEM_RIGHT
		);
		
		if(hdw.weaponstatus[0]&MAUG3F_MAG1SHRED)sb.Fill(redCol,-38,-13.5,22,0.5,sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_ITEM_RIGHT);
		if(hdw.weaponstatus[0]&MAUG3F_MAG2SHRED)sb.Fill(redCol,-38,-10.5,22,0.5,sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_ITEM_RIGHT);
		if(hdw.weaponstatus[0]&MAUG3F_MAG3SHRED)sb.Fill(redCol,-38,-7.5,22,0.5,sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_ITEM_RIGHT);
		
		sb.drawwepnum(lod,50,posy:-13);
		if(hdw.weaponstatus[MAUG3S_CHAMBER1]==2){
			sb.drawrect(-27,-17,3,1);
			lod++;
		}
		sb.drawwepnum(lod2,50,posy:-10);
		if(hdw.weaponstatus[MAUG3S_CHAMBER2]==2){
			sb.drawrect(-23,-17,3,1);
			lod2++;
		}
		sb.drawwepnum(lod3,50,posy:-7);
		if(hdw.weaponstatus[MAUG3S_CHAMBER3]==2){
			sb.drawrect(-19,-17,3,1);
			lod3++;
		}
		if(bat){
			sb.drawwepnum(hdw.weaponstatus[MAUG3S_BATTERY],20,posy:-3);
		}else if(!hdw.weaponstatus[MAUG3S_BATTERY])sb.drawstring(
			sb.mamountfont,"00000",(-16,-8),
			sb.DI_TEXT_ALIGN_RIGHT|sb.DI_TRANSLATABLE|sb.DI_SCREEN_CENTER_BOTTOM,
			Font.CR_DARKGRAY
		);
	}
		
	override string gethelptext(){
		if(maug03open)return
		WEPHELP_FIRE.."  Close\n"
		..WEPHELP_ALTFIRE.."  Cycle slot \(Hold "..WEPHELP_ZOOM.." to reverse\)\n"
		..WEPHELP_UNLOAD.."  Unload slot\n"
		..WEPHELP_RELOAD.."  Load slot\n"
		;
		return
		WEPHELP_FIRESHOOT
		..WEPHELP_UNLOAD.."/"..WEPHELP_RELOAD.."  Open\n"
		..WEPHELP_FIREMODE.."  Burst/Auto\n"
		..WEPHELP_MAGMANAGER
		;
	}
	
	override void DrawSightPicture(
		HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl,
		bool sightbob,vector2 bob,double fov,bool scopeview,actor hpc
	){
		if(WAN_MAUG03(hdw).maug03open)return;
		int scaledyoffset=47;
		double dotoff=max(abs(bob.x),abs(bob.y));
		if(dotoff<40){
			string whichdot=sb.ChooseReflexReticle(hdw.weaponstatus[MAUG3S_DOT]);
			sb.drawimage(
				whichdot,(0,0)+bob*1.1,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER,
				alpha:0.8-dotoff*0.01,
				col:0xFF000000|sb.crosshaircolor.GetInt()
			);
		}
		sb.drawimage(
			"mau3site",(0,0)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER
		);
	}
	
	action void chkChamber1(){
		if(invoker.weaponstatus[MAUG3S_CHAMBER1]==1)invoker.weaponstatus[MAUG3S_CHAMBER1]=0;
			else if(invoker.weaponstatus[MAUG3S_MAG1]>0){
				if(invoker.weaponstatus[MAUG3S_MAG1]==51){
					invoker.weaponstatus[MAUG3S_MAG1]=50;
				}
				invoker.weaponstatus[0]|=MAUG3F_MAG1SHRED;
				invoker.weaponstatus[MAUG3S_MAG1]--;
				invoker.weaponstatus[MAUG3S_CHAMBER1]=2;
			}
	}
	
	action void chkChamber2(){
		if(invoker.weaponstatus[MAUG3S_CHAMBER2]==1)invoker.weaponstatus[MAUG3S_CHAMBER2]=0;
			else if(invoker.weaponstatus[MAUG3S_MAG2]>0){
				if(invoker.weaponstatus[MAUG3S_MAG2]==51){
					invoker.weaponstatus[MAUG3S_MAG2]=50;
				}
				invoker.weaponstatus[0]|=MAUG3F_MAG2SHRED;
				invoker.weaponstatus[MAUG3S_MAG2]--;
				invoker.weaponstatus[MAUG3S_CHAMBER2]=2;
			}
	}
	
	action void chkChamber3(){
		if(invoker.weaponstatus[MAUG3S_CHAMBER3]==1)invoker.weaponstatus[MAUG3S_CHAMBER3]=0;
			else if(invoker.weaponstatus[MAUG3S_MAG3]>0){
				if(invoker.weaponstatus[MAUG3S_MAG3]==51){
					invoker.weaponstatus[MAUG3S_MAG3]=50;
				}
				invoker.weaponstatus[0]|=MAUG3F_MAG3SHRED;
				invoker.weaponstatus[MAUG3S_MAG3]--;
				invoker.weaponstatus[MAUG3S_CHAMBER3]=2;
			}
	}
	
	action void A_ReadyOpen(){
		A_WeaponReady(WRF_NOFIRE|WRF_ALLOWUSER3);
		if(justpressed(BT_ALTATTACK))setweaponstate("changeselectedmag");
		else if(justpressed(BT_RELOAD)){
			if(
				(
					invoker.weaponstatus[MAUG3S_MAG1]>=0
					&&invoker.weaponstatus[MAUG3S_MAG2]>=0
					&&invoker.weaponstatus[MAUG3S_MAG3]>=0
					&&invoker.weaponstatus[MAUG3S_BATTERY]>=0
				)||(
					!countinv("HD4mMag")
				)
			)setweaponstate("closeit");//close
			else {//load
				switch (invoker.weaponstatus[MAUG3S_SELECTED]){
					case 1:
						setweaponstate("loadmag1");
						break;
					case 2:
						setweaponstate("loadmag2");
						break;
					case 3:
						setweaponstate("loadmag3");
						break;
					case 4:
						setweaponstate("loadcell");
					default:
						break;
				}
			}
		}else if(justpressed(BT_ATTACK))setweaponstate("closeit");//close
		else if(justpressed(BT_UNLOAD)){
			switch (invoker.weaponstatus[MAUG3S_SELECTED]){
				case 1:
					setweaponstate("unloadmag1");
					break;
				case 2:
					setweaponstate("unloadmag2");
					break;
				case 3:
					setweaponstate("unloadmag3");
					break;
				case 4:
					setweaponstate("unloadcell");
				default:
					break;
			}
		}
	}
	
	states{
	select0:
		MAU3 A 0{
			invoker.maug03open=false;
			A_SetHelpText();
		}
		goto select0big;
	deselect0:
		MAU3 A 0 A_JumpIf(!invoker.maug03open,"deselect0big");
		MAU3 A 4 offset(-20,48){
			A_StartSound("weapons/maug03_open2",CHAN_WEAPON,CHANF_OVERLAP);
			invoker.maug03open=false;
			A_SetHelpText();
		}
		MAU3 A 2 offset(-13,40);
		MAU3 A 1 offset(-8,37);
		MAU3 A 1 offset(-3,34);
		MAU3 A 1 offset(0,33);
		goto readyend;
		
	ready:
		MAU3 A 0;
		---- A 0 A_JumpIf(invoker.maug03open,"readyopen");
		MAU3 A 1 A_WeaponReady(WRF_ALL);
		goto readyend;
	
	fire:
		MAU3 A 0{
			if(
				(invoker.weaponstatus[0]&MAUG3F_FULLAUTO)
			)setweaponstate("shoot");
			else setweaponstate("shootalt");
		}
	hold:
		MAU3 B 1{
			if(
				!(invoker.weaponstatus[0]&MAUG3F_FULLAUTO)
			)setweaponstate("nope");
			else if(
				invoker.weaponstatus[MAUG3S_CHAMBER1]<1
				&&invoker.weaponstatus[MAUG3S_CHAMBER2]<1
				&&invoker.weaponstatus[MAUG3S_CHAMBER3]<1
			)setweaponstate("nope");
		}goto shoot;

	shoot:
		MAU3 A 0 {
			if(invoker.weaponstatus[MAUG3S_CHAMBER1]==2){
				if(random(0,65))A_Overlay(11,"flash1");
				else A_Overlay(11,"flash1oops");
				invoker.weaponstatus[MAUG3S_HEAT]+=random(2,4);
			}
		}
		MAU3 B 1 A_WeaponReady(WRF_NOFIRE);
		MAU3 A 1 {
			if(invoker.weaponstatus[MAUG3S_BATTERY]>0){
				if((!(invoker.weaponstatus[0]&MAUG3F_MAG1SHRED))&&invoker.weaponstatus[MAUG3S_MAG1]>0){
						A_StartSound("weapons/maug03_shred",CHAN_WEAPON,CHANF_OVERLAP);
						if(!random(0,2))invoker.weaponstatus[MAUG3S_BATTERY]--;
				}
				invoker.chkChamber1();
			}
		}
		MAU3 A 0 {
			if(invoker.weaponstatus[MAUG3S_CHAMBER2]==2){
				if(random(0,65))A_Overlay(12,"flash2");
				else A_Overlay(12,"flash2oops");
				invoker.weaponstatus[MAUG3S_HEAT]+=random(2,4);
			}
		}
		MAU3 B 1 A_WeaponReady(WRF_NOFIRE);
		MAU3 A 1 {
			if(invoker.weaponstatus[MAUG3S_BATTERY]>0){
				if((!(invoker.weaponstatus[0]&MAUG3F_MAG2SHRED))&&invoker.weaponstatus[MAUG3S_MAG2]>0){
					A_StartSound("weapons/maug03_shred",CHAN_WEAPON,CHANF_OVERLAP);
					if(!random(0,2))invoker.weaponstatus[MAUG3S_BATTERY]--;
				}
				invoker.chkChamber2();
			}
		}
		MAU3 A 0 {
			if(invoker.weaponstatus[MAUG3S_CHAMBER3]==2){
				if(random(0,65))A_Overlay(13,"flash3");
				else A_Overlay(13,"flash3oops");
				invoker.weaponstatus[MAUG3S_HEAT]+=random(2,4);
			}
		}
		MAU3 B 1 A_WeaponReady(WRF_NOFIRE);
		MAU3 A 1 {
			if(invoker.weaponstatus[MAUG3S_BATTERY]>0){
				if((!(invoker.weaponstatus[0]&MAUG3F_MAG3SHRED))&&invoker.weaponstatus[MAUG3S_MAG3]>0){
					A_StartSound("weapons/maug03_shred",CHAN_WEAPON,CHANF_OVERLAP);
					if(!random(0,2))invoker.weaponstatus[MAUG3S_BATTERY]--;
				}
				invoker.chkChamber3();
				if(!random(0,100))invoker.weaponstatus[MAUG3S_BATTERY]--;
				A_Refire();
			}
		}
		goto nope;
	shootalt:
		MAU3 B 1;
		MAU3 B 1{
			if(invoker.weaponstatus[MAUG3S_CHAMBER1]==2){
				if(random(0,50))A_Overlay(11,"flash1");
				else A_Overlay(11,"flash1oops");
				invoker.weaponstatus[MAUG3S_HEAT]+=random(2,4);
			}
			if(invoker.weaponstatus[MAUG3S_CHAMBER2]==2){
				if(random(0,50))A_Overlay(12,"flash2");
				else A_Overlay(12,"flash2oops");
				invoker.weaponstatus[MAUG3S_HEAT]+=random(2,4);
			}
			if(invoker.weaponstatus[MAUG3S_CHAMBER3]==2){
				if(random(0,50))A_Overlay(13,"flash3");
				else A_Overlay(13,"flash3oops");
				invoker.weaponstatus[MAUG3S_HEAT]+=random(2,4);
			}
		}
		MAU3 A 2{
			if(invoker.weaponstatus[MAUG3S_BATTERY]>0){
				if((!(invoker.weaponstatus[0]&MAUG3F_MAG1SHRED))&&invoker.weaponstatus[MAUG3S_MAG1]>0){
					A_StartSound("weapons/maug03_shred",CHAN_WEAPON,CHANF_OVERLAP);
					if(!random(0,2))invoker.weaponstatus[MAUG3S_BATTERY]--;
				}
				invoker.chkChamber1();
			}
			if(invoker.weaponstatus[MAUG3S_BATTERY]>0){
				if((!(invoker.weaponstatus[0]&MAUG3F_MAG2SHRED))&&invoker.weaponstatus[MAUG3S_MAG2]>0){
					A_StartSound("weapons/maug03_shred",CHAN_WEAPON,CHANF_OVERLAP);
					if(!random(0,2))invoker.weaponstatus[MAUG3S_BATTERY]--;
				}
				invoker.chkChamber2();
			}
			if(invoker.weaponstatus[MAUG3S_BATTERY]>0){
				if((!(invoker.weaponstatus[0]&MAUG3F_MAG3SHRED))&&invoker.weaponstatus[MAUG3S_MAG3]>0){
					A_StartSound("weapons/maug03_shred",CHAN_WEAPON,CHANF_OVERLAP);
					if(!random(0,2))invoker.weaponstatus[MAUG3S_BATTERY]--;
				}
				invoker.chkChamber3();
				if(!random(0,100))invoker.weaponstatus[MAUG3S_BATTERY]--;
			}
		}
		goto nope;
		
	flash1:
		MAU3 C 1 bright{
			invoker.weaponstatus[MAUG3S_CHAMBER1]=0;
			A_Light1();
			HDBulletActor.FireBullet(self,
				"HDB_426",
				xyofs:1,spread:1.5,aimoffx:-0.1
			);
			HDFlashAlpha(14,layer:11);
			A_ZoomRecoil(0.96);
			A_StartSound("weapons/maug03_fire",CHAN_WEAPON,CHANF_OVERLAP);
			A_MuzzleClimb(
					0.1,-frandom(0,0.1),
					-0.2,-frandom(0.2,0.3),
					frandom(0.2,0.3),-frandom(0.7,1.2)
				);
			A_AlertMonsters();
		}
		TNT1 A 0 A_Light0();
		stop;
	flash1oops:
		TNT1 A 1 {
			A_StartSound("weapons/bigcrack",8,CHANF_OVERLAP);
			A_SpawnItemEx("HDSmokeChunk",12,0,self.height-12,4,frandom(-2,2),frandom(2,4));
			invoker.weaponstatus[MAUG3S_CHAMBER1]=1;
			A_MuzzleClimb(
					frandom(0.1,0.3),0.0,
					frandom(0.2,0.4),0.0,
					frandom(1.2,2.6),frandom(0.8,1.8)
				);
			if(random(0,1))damagemobj(invoker,self,1,"hot",DMG_NO_ARMOR);
		}
		stop;
	flash2:
		MAU3 D 1 bright{
			invoker.weaponstatus[MAUG3S_CHAMBER2]=0;
			A_Light1();
			HDBulletActor.FireBullet(self,
				"HDB_426",
				spread:1.5
			);
			HDFlashAlpha(14,layer:12);
			A_ZoomRecoil(0.96);
			A_StartSound("weapons/maug03_fire",CHAN_WEAPON,CHANF_OVERLAP);
			A_MuzzleClimb(
					-frandom(-0.01,0.05),-frandom(0,0.1),
					-0.01,-frandom(0.2,0.3),
					-frandom(0.1,0.2),-frandom(0.7,1.2)
				);
			A_AlertMonsters();
		}
		TNT1 A 0 A_Light0();
		stop;
	flash2oops:
		TNT1 A 1 {
			A_StartSound("weapons/bigcrack",8,CHANF_OVERLAP);
			A_SpawnItemEx("HDSmokeChunk",12,0,self.height-12,4,frandom(-2,2),frandom(2,4));
			invoker.weaponstatus[MAUG3S_CHAMBER2]=1;
			A_MuzzleClimb(
					-frandom(-0.1,0.1),0.0,
					-frandom(-0.2,0.2),0.0,
					-frandom(-0.6,0.6),frandom(0.8,1.8)
				);
			if(random(0,1))damagemobj(invoker,self,1,"hot",DMG_NO_ARMOR);
		}
		stop;
	flash3:
		MAU3 E 1 bright{
			invoker.weaponstatus[MAUG3S_CHAMBER3]=0;
			A_Light1();
			HDBulletActor.FireBullet(self,
				"HDB_426",
				xyofs:-1,spread:1.5,aimoffx:0.1
			);
			HDFlashAlpha(14,layer:13);
			A_ZoomRecoil(0.96);
			A_StartSound("weapons/maug03_fire",CHAN_WEAPON,CHANF_OVERLAP);
			A_MuzzleClimb(
					-0.1,-frandom(0,0.1),
					-0.2,-frandom(0.2,0.3),
					-frandom(0.2,0.3),-frandom(0.7,1.2)
				);
			A_AlertMonsters();
		}
		TNT1 A 0 A_Light0();
		stop;
	flash3oops:
		TNT1 A 1 {
			A_StartSound("weapons/bigcrack",8,CHANF_OVERLAP);
			A_SpawnItemEx("HDSmokeChunk",12,0,self.height-12,4,frandom(-2,2),frandom(2,4));
			invoker.weaponstatus[MAUG3S_CHAMBER3]=1;
			A_MuzzleClimb(
					-frandom(0.1,0.3),0.0,
					-frandom(0.2,0.4),0.0,
					-frandom(1.2,2.6),frandom(0.8,1.8)
				);
			if(random(0,1))damagemobj(invoker,self,1,"hot",DMG_NO_ARMOR);
		}
		stop;
	
	firemode:
		MAU3 B 1{
			invoker.weaponstatus[0]^=MAUG3F_FULLAUTO;
			A_WeaponReady(WRF_NONE);
		}
	firemodehold:
		MAU3 A 1{
			A_WeaponReady(WRF_NONE);
		}
		MAU3 A 0 A_JumpIf(pressingfiremode(),"firemodehold");
		goto nope;
		
	user3:
		MAU3 A 0 A_MagManager("HD4mMag");
		goto ready;
		
	reload:
	unload:
		MAU3 A 1 offset(0,33);
		MAU3 A 1 offset(-3,34);
		MAU3 A 1 offset(-8,37);
		MAU3 A 1 offset(-13,40);
		MAU3 A 2 offset(-20,48);
		MAU3 F 4 offset(1,1){
			A_MuzzleClimb(-0.3,-0.3);
			A_StartSound("weapons/maug03_open1",CHAN_WEAPON);
			invoker.maug03open=true;
			A_SetHelpText();
		}
		MAU3 F 0 { //auto eject empty, shredded mags
			int inmag=invoker.weaponstatus[MAUG3S_MAG1];
			bool shredded=invoker.weaponstatus[0]&MAUG3F_MAG1SHRED;
			if (shredded&&inmag==0){
				invoker.weaponstatus[MAUG3S_MAG1]=-1;
				A_StartSound("weapons/bigcrack",8,CHANF_OVERLAP);
				invoker.weaponstatus[0]&=~MAUG3F_MAG1SHRED;
				A_SpawnItemEx("WAN_4mmMagRuined",
					cos(pitch)*4,-0.1,height*0.72-sin(pitch)*4,
					frandom(0.9,1.1),frandom(-0.2,0.2),0,
					0,SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
				);
			}
			inmag=invoker.weaponstatus[MAUG3S_MAG2];
			shredded=invoker.weaponstatus[0]&MAUG3F_MAG2SHRED;
			if (shredded&&inmag==0){
				invoker.weaponstatus[MAUG3S_MAG2]=-1;
				A_StartSound("weapons/bigcrack",8,CHANF_OVERLAP);
				invoker.weaponstatus[0]&=~MAUG3F_MAG2SHRED;
				A_SpawnItemEx("WAN_4mmMagRuined",
					cos(pitch)*4,0,height*0.72-sin(pitch)*4,
					frandom(0.9,1.1),frandom(-0.2,0.2),0,
					0,SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
				);
			}
			inmag=invoker.weaponstatus[MAUG3S_MAG3];
			shredded=invoker.weaponstatus[0]&MAUG3F_MAG3SHRED;
			if (shredded&&inmag==0){
				invoker.weaponstatus[MAUG3S_MAG3]=-1;
				A_StartSound("weapons/bigcrack",8,CHANF_OVERLAP);
				invoker.weaponstatus[0]&=~MAUG3F_MAG3SHRED;
				A_SpawnItemEx("WAN_4mmMagRuined",
					cos(pitch)*4,0.1,height*0.72-sin(pitch)*4,
					frandom(0.9,1.1),frandom(-0.2,0.2),0,
					0,SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
				);
			}
		}
	readyopen:
		MAU3 F 1 {
			A_ReadyOpen();
		}
		goto readyend;
	closeit:
		MAU3 A 4 offset(-20,48){
			A_StartSound("weapons/maug03_open2",CHAN_WEAPON,CHANF_OVERLAP);
			invoker.maug03open=false;
			A_SetHelpText();
		}
		MAU3 A 2 offset(-13,40);
		MAU3 A 1 offset(-8,37);
		MAU3 A 1 offset(-3,34);
		MAU3 A 1 offset(0,33);
		goto readyend;
	changeselectedmag:
		MAU3 F 1 offset(0,33){
			if(pressingzoom()){
				if(invoker.weaponstatus[MAUG3S_SELECTED]>1)invoker.weaponstatus[MAUG3S_SELECTED]--;
				else invoker.weaponstatus[MAUG3S_SELECTED]=4;
			}else{
				if(invoker.weaponstatus[MAUG3S_SELECTED]<4)invoker.weaponstatus[MAUG3S_SELECTED]++;
				else invoker.weaponstatus[MAUG3S_SELECTED]=1;
			}
			A_StartSound("weapons/maug03_cycle",CHAN_WEAPON);
		}goto readyopen;
	//only after making the reloading code did i realize it could probably be done better; i'll get around to it at some point
	unloadmag1:
		MAU3 F 4 offset(-1,33){
			if(invoker.weaponstatus[MAUG3S_MAG1]<0)setweaponstate("unloadend");
		}
		MAU3 F 0 offset(-1,33) A_StartSound("weapons/maug03_unload",CHAN_WEAPON);
		MAU3 F 20 offset(-2,35){
			int inmag=invoker.weaponstatus[MAUG3S_MAG1];
			invoker.weaponstatus[MAUG3S_MAG1]=-1;
			bool shredded=invoker.weaponstatus[0]&MAUG3F_MAG1SHRED;
			if (shredded){
				A_StartSound("weapons/bigcrack",8,CHANF_OVERLAP);
				invoker.weaponstatus[0]&=~MAUG3F_MAG1SHRED;
				if(inmag>0){
					HDPickup.DropItem(self,"FourMilAmmo",(max(1,inmag*0.75)));
				}
				A_SpawnItemEx("WAN_4mmMagRuined",
					cos(pitch)*4,-0.1,height*0.72-sin(pitch)*4,
					frandom(0.9,1.1),frandom(-0.2,0.2),0,
					0,SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
				);
			}else if(
				!PressingUnload()&&!PressingReload()
				||A_JumpIfInventory("HD4mMag",0,"null")
			){
				HDMagAmmo.SpawnMag(self,"HD4mMag",inmag);
				A_SetTics(8);
			}else{
				HDMagAmmo.GiveMag(self,"HD4mMag",inmag);
				A_StartSound("weapons/pocket",CHAN_WEAPON);
				if(inmag<51)A_Log(HDCONST_426MAGMSG,true);
			}
		}
		MAU3 F 0 {invoker.weaponstatus[MAUG3S_SELECTED]++;
		}goto unloadend;
	unloadmag2:
		MAU3 F 4 offset(-1,33){
			if(invoker.weaponstatus[MAUG3S_MAG2]<0)setweaponstate("unloadend");
		}
		MAU3 F 0 offset(-1,33) A_StartSound("weapons/maug03_unload",CHAN_WEAPON);
		MAU3 F 20 offset(-2,35){
			int inmag=invoker.weaponstatus[MAUG3S_MAG2];
			invoker.weaponstatus[MAUG3S_MAG2]=-1;
			bool shredded=invoker.weaponstatus[0]&MAUG3F_MAG2SHRED;
			if (shredded){
				A_StartSound("weapons/bigcrack",8,CHANF_OVERLAP);
				invoker.weaponstatus[0]&=~MAUG3F_MAG2SHRED;
				if(inmag>0){
					HDPickup.DropItem(self,"FourMilAmmo",(max(1,inmag*0.75)));
				}
				A_SpawnItemEx("WAN_4mmMagRuined",
					cos(pitch)*4,0,height*0.72-sin(pitch)*4,
					frandom(0.9,1.1),frandom(-0.2,0.2),0,
					0,SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
				);
			}else if(
				!PressingUnload()&&!PressingReload()
				||A_JumpIfInventory("HD4mMag",0,"null")
			){
				HDMagAmmo.SpawnMag(self,"HD4mMag",inmag);
				A_SetTics(8);
			}else{
				HDMagAmmo.GiveMag(self,"HD4mMag",inmag);
				A_StartSound("weapons/pocket",CHAN_WEAPON);
				if(inmag<51)A_Log(HDCONST_426MAGMSG,true);
			}
		}
		MAU3 F 0 {invoker.weaponstatus[MAUG3S_SELECTED]++;
		}goto unloadend;
	unloadmag3:
		MAU3 F 4 offset(-1,33){
			if(invoker.weaponstatus[MAUG3S_MAG3]<0)setweaponstate("unloadend");
		}
		MAU3 F 0 offset(-1,33) A_StartSound("weapons/maug03_unload",CHAN_WEAPON);
		MAU3 F 20 offset(-2,35){
			int inmag=invoker.weaponstatus[MAUG3S_MAG3];
			invoker.weaponstatus[MAUG3S_MAG3]=-1;
			bool shredded=invoker.weaponstatus[0]&MAUG3F_MAG3SHRED;
			if (shredded){
				A_StartSound("weapons/bigcrack",8,CHANF_OVERLAP);
				invoker.weaponstatus[0]&=~MAUG3F_MAG3SHRED;
				if(inmag>0){
					HDPickup.DropItem(self,"FourMilAmmo",(max(1,inmag*0.75)));
				}
				A_SpawnItemEx("WAN_4mmMagRuined",
					cos(pitch)*4,0.1,height*0.72-sin(pitch)*4,
					frandom(0.9,1.1),frandom(-0.2,0.2),0,
					0,SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
				);
			}else if(
				!PressingUnload()&&!PressingReload()
				||A_JumpIfInventory("HD4mMag",0,"null")
			){
				HDMagAmmo.SpawnMag(self,"HD4mMag",inmag);
				A_SetTics(8);
			}else{
				HDMagAmmo.GiveMag(self,"HD4mMag",inmag);
				A_StartSound("weapons/pocket",CHAN_WEAPON);
				if(inmag<51)A_Log(HDCONST_426MAGMSG,true);
			}
		}MAU3 F 0 {invoker.weaponstatus[MAUG3S_SELECTED]=1;
		}goto unloadend;
	unloadcell:
		MAU3 F 4 offset(-1,33){
			if(invoker.weaponstatus[MAUG3S_BATTERY]<0)setweaponstate("unloadend");
		}
		MAU3 F 0 offset(-1,33) A_StartSound("weapons/maug03_unload",CHAN_WEAPON);
		MAU3 F 20 offset(-2,35){
			int btt=invoker.weaponstatus[MAUG3S_BATTERY];
			invoker.weaponstatus[MAUG3S_BATTERY]=-1;
			if(
				!PressingUnload()
				&&!PressingAltReload()
				&&!PressingReload()
			){
				A_SetTics(4);
				HDMagAmmo.SpawnMag(self,"HDBattery",btt);
				
			}else{
				A_StartSound("weapons/pocket",CHAN_WEAPON);
				HDMagAmmo.GiveMag(self,"HDBattery",btt);
			}
		}goto unloadend;
		
	loadmag1:
		MAU3 F 0{
			if(invoker.weaponstatus[MAUG3S_MAG1]>=0)setweaponstate("unloadend");
			if(HDMagAmmo.NothingLoaded(self,"HD4mMag"))setweaponstate("unloadend");
		}
		MAU3 F 10 offset(-1,33){
			let zmag=HD4mMag(findinventory("HD4mMag"));
			if(!zmag){setweaponstate("unloadend");return;}
			A_StartSound("weapons/pocket",CHAN_WEAPON);
		}
		MAU3 F 6 offset(-2,35)A_StartSound("weapons/maug03_load",CHAN_WEAPON);
		MAU3 F 3 offset(-1,32){
			let zmag=HD4mMag(findinventory("HD4mMag"));
			if(!zmag){setweaponstate("unloadend");return;}
			invoker.weaponstatus[MAUG3S_MAG1]=zmag.TakeMag(true);
			A_StartSound("weapons/rifleclick2",CHAN_WEAPON);
		}
		MAU3 F 0 {invoker.weaponstatus[MAUG3S_SELECTED]++;
		}goto unloadend;
	loadmag2:
		MAU3 F 0{
			if(invoker.weaponstatus[MAUG3S_MAG2]>=0)setweaponstate("unloadend");
			if(HDMagAmmo.NothingLoaded(self,"HD4mMag"))setweaponstate("unloadend");
		}
		MAU3 F 10 offset(-1,33){
			let zmag=HD4mMag(findinventory("HD4mMag"));
			if(!zmag){setweaponstate("unloadend");return;}
			A_StartSound("weapons/pocket",CHAN_WEAPON);
		}
		MAU3 F 6 offset(-2,35)A_StartSound("weapons/maug03_load",CHAN_WEAPON);
		MAU3 F 3 offset(-1,32){
			let zmag=HD4mMag(findinventory("HD4mMag"));
			if(!zmag){setweaponstate("unloadend");return;}
			invoker.weaponstatus[MAUG3S_MAG2]=zmag.TakeMag(true);
			A_StartSound("weapons/rifleclick2",CHAN_WEAPON);
		}
		MAU3 F 0 {invoker.weaponstatus[MAUG3S_SELECTED]++;
		}goto unloadend;
	loadmag3:
		MAU3 F 0{
			if(invoker.weaponstatus[MAUG3S_MAG3]>=0)setweaponstate("unloadend");
			if(HDMagAmmo.NothingLoaded(self,"HD4mMag"))setweaponstate("unloadend");
		}
		MAU3 F 10 offset(-1,33){
			let zmag=HD4mMag(findinventory("HD4mMag"));
			if(!zmag){setweaponstate("unloadend");return;}
			A_StartSound("weapons/pocket",CHAN_WEAPON);
		}
		MAU3 F 6 offset(-2,35)A_StartSound("weapons/maug03_load",CHAN_WEAPON);
		MAU3 F 3 offset(-1,32){
			let zmag=HD4mMag(findinventory("HD4mMag"));
			if(!zmag){setweaponstate("unloadend");return;}
			invoker.weaponstatus[MAUG3S_MAG3]=zmag.TakeMag(true);
			A_StartSound("weapons/rifleclick2",CHAN_WEAPON);
		}
		MAU3 F 0 {invoker.weaponstatus[MAUG3S_SELECTED]=1;
		}goto unloadend;
	loadcell:
		MAU3 F 0{
			if(invoker.weaponstatus[MAUG3S_BATTERY]>=0)setweaponstate("unloadend");
			if(HDMagAmmo.NothingLoaded(self,"HDBattery"))setweaponstate("unloadend");
		}
		MAU3 F 10 offset(-1,33){
			let bat=HDMagAmmo(findinventory("HDBattery"));
			if(!bat){setweaponstate("unloadend");return;}
			A_StartSound("weapons/pocket",CHAN_WEAPON);
		}
		MAU3 F 6 offset(-2,35)A_StartSound("weapons/maug03_load",CHAN_WEAPON);
		MAU3 F 3 offset(-1,32){
			let bat=HDMagAmmo(findinventory("HDBattery"));
			if(!bat){setweaponstate("unloadend");return;}
			invoker.weaponstatus[MAUG3S_BATTERY]=bat.TakeMag(true);
			A_StartSound("weapons/rifleclick2",CHAN_WEAPON);
		}goto unloadend;
		
	unloadend:
		MAU3 F 2 offset(-1,33);
		goto readyopen;
		
	spawn:
		MAU3 Z -1;
		stop;
	}
	
	override void InitializeWepStats(bool idfa){
		weaponstatus[MAUG3S_MAG1]=51;
		weaponstatus[MAUG3S_MAG2]=51;
		weaponstatus[MAUG3S_MAG3]=51;
		weaponstatus[MAUG3S_CHAMBER1]=0;
		weaponstatus[MAUG3S_CHAMBER2]=0;
		weaponstatus[MAUG3S_CHAMBER3]=0;
		weaponstatus[MAUG3S_BATTERY]=20;
		weaponstatus[MAUG3S_HEAT]=0;
		weaponstatus[MAUG3S_SELECTED]=1;
		
		weaponstatus[0]&=~MAUG3F_MAG1SHRED;
		weaponstatus[0]&=~MAUG3F_MAG2SHRED;
		weaponstatus[0]&=~MAUG3F_MAG3SHRED;
	}
	
	override void loadoutconfigure(string input){
		int firemode=getloadoutvar(input,"firemode",1);
		if(firemode>0)weaponstatus[0]&=~MAUG3F_FULLAUTO;
		else weaponstatus[0]|=MAUG3F_FULLAUTO;
	}
}

enum maug3status{
	MAUG3S_FLAGS=0,
	MAUG3S_CHAMBER1=1,//0 empty, 2 loaded, 1 broken
	MAUG3S_CHAMBER2=2,
	MAUG3S_CHAMBER3=3,
	MAUG3S_MAG1=4,//-1 is no mag, 0 is empty mag
	MAUG3S_MAG2=5,
	MAUG3S_MAG3=6,
	MAUG3S_BATTERY=7,
	MAUG3S_HEAT=8,
	MAUG3S_DOT=9,
	MAUG3S_SELECTED=10,

	MAUG3F_FULLAUTO=1,
	MAUG3F_UNLOADONLY=2,
	MAUG3F_MAG1SHRED=4,
	MAUG3F_MAG2SHRED=8,
	MAUG3F_MAG3SHRED=16,
};

class WAN_MAUG03_BROKEN:HDActor{
	default{
		tag "MAUG-03B (Broken)";
		scale 0.7;
	}
	states{
	spawn:
		MAU3 X -1;
		stop;
	}
}

class WAN_4mmMagRuined:HDActor{
	default{
		tag "4.26mm UAC Standard magazine (Ruined)";
		translation "0:255=%[0.00,0.00,0.00]:[0.30,0.22,0.26]";
		scale 0.8;
		radius 2;
		height 1;
	}
	states{
	spawn:
		ZMAG C -1;
		stop;
	}
}
