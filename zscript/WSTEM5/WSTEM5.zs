// ------------------------------------------------------------
// WSTE-M5 Combat Shotgun
// ------------------------------------------------------------

//I didn't comment ANY of this, I will definitely regret this later - wan
const HDLD_WSTEM5="wst";
class Wan_WSTEM5:HDShotgun{
	default{
		//$Category "Weapons/Hideous Destructor"
		//$Title "Slayer"
		//$Sprite "SLAYA0"

		+hdweapon.fitsinbackpack
		+hdweapon.reverseguninertia
		weapon.selectionorder 30;
		weapon.slotnumber 3;
		weapon.slotpriority 2.6;
		//inventory.pickupmessage "You got a WSTE-M5 Combat Shotgun!";
		inventory.pickupsound "weapons/WsteM5_pickup";
		obituary "$WAN_OBIT_WSTEM5";
		weapon.bobrangex 0.1;
		weapon.bobrangey 0.6;
		weapon.bobspeed 2.5;
		weapon.bobstyle "normal";
		scale 0.6;
		hdweapon.barrelsize 18,1,1;
		hdweapon.ammo1 "",4;
		tag "$WAN_TAG_WSTEM5";
		hdweapon.refid HDLD_WSTEM5;
		hdweapon.loadoutcodes "
			\cuslug - 0/1, Chamber for slugs";
	}
		bool wronghand;
		override string pickupmessage() {
			bool slug=weaponstatus[0]&WSTEF_SLUGS;
			return Stringtable.Localize(slug?"$WAN_PICK_WSTEM5_SLUG":"$WAN_PICK_WSTEM5");}
		override bool AddSpareWeapon(actor newowner){return AddSpareWeaponRegular(newowner);}
		override hdweapon GetSpareWeapon(actor newowner,bool reverse,bool doselect){return GetSpareWeaponRegular(newowner,reverse,doselect);}
		action void A_SwapHandguns(){
		let mwt=SpareWeapons(findinventory("SpareWeapons"));
		if(!mwt){
			setweaponstate("whyareyousmiling");
			return;
		}
		int pistindex=mwt.weapontype.find(invoker.getclassname());
		if(pistindex==mwt.weapontype.size()){
			setweaponstate("whyareyousmiling");
			return;
		}
		A_WeaponBusy();

		array<string> wepstat;
		string wepstat2="";
		mwt.weaponstatus[pistindex].split(wepstat,",");
		for(int i=0;i<wepstat.size();i++){
			if(i)wepstat2=wepstat2..",";
			wepstat2=wepstat2..invoker.weaponstatus[i];
			invoker.weaponstatus[i]=wepstat[i].toint();
		}
		mwt.weaponstatus[pistindex]=wepstat2;

		invoker.wronghand=!invoker.wronghand;
		invoker.postbeginplay();
	}
	static void Fire(actor caller,bool right,int choke=7){
		double shotpower=getshotpower();
		double spread=3.;
		double speedfactor=1.2;
		let sss=Wan_WSTEM5(caller.findinventory("Wan_WSTEM5"));
		if(sss){
			sss.shotpower=shotpower;
		}
		bool slug=sss.weaponstatus[0]&WSTEF_SLUGS;
		spread=slug?6:10;
		speedfactor=1.01;

		spread*=shotpower;
		speedfactor*=shotpower;
		vector2 barreladjust=(0.8,-0.05);
		if(right)barreladjust=-barreladjust;
		HDBulletActor.FireBullet(caller,"HDB_wad",xyofs:barreladjust.x,aimoffx:barreladjust.y);
		let p=HDBulletActor.FireBullet(caller,slug?"HDB_Slug":"HDB_00",xyofs:barreladjust.x,
			spread:spread,aimoffx:barreladjust.y,speedfactor:speedfactor,amount:slug?1:10
		);
		distantnoise.make(p,"world/shotgunfar");
	}
	override string,double getpickupsprite(){
		return "WSTEX0",1.;
	}
	override void DrawHUDStuff(HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl){
		bool slug=hdw.weaponstatus[0]&WSTEF_SLUGS;
		if(sb.hudlevel==1){
			sb.drawimage(slug?"SLG1A0":"SHL1A0",(-47,-10),sb.DI_SCREEN_CENTER_BOTTOM);
			sb.drawnum(hpl.countinv(slug?"HDSlugAmmo":"HDShellAmmo"),-46,-8,sb.DI_SCREEN_CENTER_BOTTOM);
		}
		int loffs=-27;int rofs=-23;//loffs=-31;rofs=-18;
		sb.drawimage("STBURAUT",(-23,-17),sb.DI_SCREEN_CENTER_BOTTOM);
		/*if(hdw.weaponstatus[0]&WSTEF_DOUBLE){
			loffs=-27;rofs=-23;
			sb.drawimage("STBURAUT",(-23,-17),sb.DI_SCREEN_CENTER_BOTTOM);
		}*/
		if(hdw.weaponstatus[WSTES_CHAMBER1]>1){
			sb.drawrect(loffs,-15,3,5);
			sb.drawrect(loffs,-9,3,2);
			sb.drawrect(rofs,-15,3,5);
			sb.drawrect(rofs,-9,3,2);
		}else if(hdw.weaponstatus[WSTES_CHAMBER1]>0){
			sb.drawrect(loffs,-9,3,2);
			sb.drawrect(rofs,-9,3,2);
		}
		//sb.drawwepnum(hdw.weaponstatus[WSTES_TUBE],3,posy:-7);
		for(int i=hdw.weaponstatus[WSTES_TUBE];i>0;i--){
			sb.drawrect(-11-i*6,-3,2,1);
			sb.drawrect(-13-i*6,-3,1,1);
			sb.drawrect(-11-i*6,-5,2,1);
			sb.drawrect(-13-i*6,-5,1,1);
		}
	}
	override string gethelptext(){
		return
		WEPHELP_FIRE.."  Shoot\n"
		..WEPHELP_ALTFIRE.."  Work Lever\n"
		..WEPHELP_RELOAD.."  Reload\n"
		..WEPHELP_ALTRELOAD.."  Quick-Swap (if available)\n"
		..WEPHELP_UNLOADUNLOAD
		;
	}
	override void DrawSightPicture(
		HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl,
		bool sightbob,vector2 bob,double fov,bool scopeview,actor hpc,string whichdot
	){
		int cx,cy,cw,ch;
		[cx,cy,cw,ch]=screen.GetClipRect();
		sb.SetClipRect(
			-16+bob.x,-4+bob.y,32,12,
			sb.DI_SCREEN_CENTER
		);
		vector2 bobb=bob*3;
		bobb.y=clamp(bobb.y,-8,8);
		sb.SetClipRect(cx,cy,cw,ch);
		sb.drawimage(
			"dbbaksit",(0,0)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP
		);
	}

	override void postbeginplay(){
		super.postbeginplay();
		if(weaponstatus[0]&WSTEF_SLUGS){
			hdammotype1="HDSlugAmmo";
		}
		else{
			hdammotype1="HDShellAmmo";
		}
	}

	override void DropOneAmmo(int amt){
		if(owner){
			bool slug=weaponstatus[0]&WSTEF_SLUGS;
			amt=clamp(amt,1,10);
			owner.A_DropInventory(slug?"HDSlugAmmo":"HDShellAmmo",amt*4);
		}
	}

	override double gunmass(){
		return 6+weaponstatus[WSTES_TUBE]*0.06;
	}
	action void A_CheckPistolHand(){
		if(invoker.wronghand)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("WST2A0");
	}
	override double weaponbulk(){
		return 80+weaponstatus[WSTES_TUBE]*ENC_SHELLLOADED;
	}
	override void detachfromowner(){
		hdweapon.detachfromowner();
	}
	//so you don't switch to the hunter every IDFA in D1
	/*override void detachfromowner(){
		if(Wads.CheckNumForName("SHT2B0",wads.ns_sprites,-1,false)<0){
			weapon.detachfromowner();
		}else hdweapon.detachfromowner();
	}*/
	transient cvar swapbarrels;
	states{
	select0:
		WSTE A 0{
			if(!countinv("NulledWeapon"))invoker.wronghand=false;
			A_TakeInventory("NulledWeapon");
			A_CheckPistolHand();
			}
		#### A 0;//{invoker.swapbarrels=cvar.getcvar("hd_swapbarrels",player);}
		goto select0small;
	deselect0:
		WSTE A 0 A_CheckPistolHand();
		goto deselect0small;
	fire:
		#### A 0;
		goto ready;
	altfire:
		#### AFG 2;
		#### HI 2 { //A_MuzzleClimb(0,0, 0,0, 0,0);  each pair is an x,y value
			if(invoker.wronghand){
				A_MuzzleClimb(frandom(0.2,0.5),-frandom(0.5,1.0));
			}else A_MuzzleClimb(-frandom(0.2,0.5),-frandom(0.5,1.0));
		}
		#### H 0{
			if (!pressingaltfire())setweaponstate("altholdaltend");
		}
	althold:
		#### I 1 A_WeaponReady(WRF_NOFIRE);
		#### I 1 {
			A_ClearRefire();
			if(pressingunload()){
				if(invoker.weaponstatus[WSTES_CHAMBER1]>0){
					invoker.weaponstatus[0]|=WSTEF_JUSTUNLOAD;
					return resolvestate("loadchamber");
				}else
					return resolvestate("altholdaltend");
			}
			if(pressingreload()&&invoker.weaponstatus[WSTES_CHAMBER1]==0){
				if(!(invoker.weaponstatus[0]&WSTEF_SLUGS)&&(countinv("HDShellAmmo"))>1||invoker.weaponstatus[0]&WSTEF_SLUGS&&(countinv("HDSlugAmmo")>1)){
					invoker.weaponstatus[0]&=~WSTEF_JUSTUNLOAD;
					return resolvestate("loadchamber");
				}else
					return resolvestate("altholdaltend");
			}else if (pressingreload())return resolvestate("altholdaltend");
			if(pressingaltfire())return resolvestate("althold");
			return resolvestate("altholdend");
		}
	loadchamber:
		#### I 1 offset(0,36) A_ClearRefire();
		#### I 1 offset(0,50);
		#### I 1 offset(0,66);
		#### I 1 offset(0,74) A_StartSound("weapons/pocket",9);
		#### I 1 offset(0,85) A_MuzzleClimb(frandom(-0.2,0.2),0.2,frandom(-0.2,0.2),0.2,frandom(-0.2,0.2),0.2);
		#### I 2 offset(0,95);
		#### I 2 offset(0,110) A_StartSound("weapons/WsteM5_open");
		#### I 16 A_StartSound("weapons/pocket",9);
		#### I 4 offset(0,112){
			A_StartSound("weapons/WsteM5_load",8);
			if(invoker.weaponstatus[0]&WSTEF_JUSTUNLOAD){
				int chm=invoker.weaponstatus[WSTES_CHAMBER1];
				bool slug=invoker.weaponstatus[0]&WSTEF_SLUGS;
				invoker.weaponstatus[WSTES_CHAMBER1]=0;
				if(chm<2){
					A_SpawnItemEx(slug?"HDSpentSlug":"HDSpentShell",
						cos(pitch)*5,frandom(-0.7,-1.0),height-10-sin(pitch)*5,
						vel.x,vel.y,vel.z,
						0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
					);
					A_SpawnItemEx(slug?"HDSpentSlug":"HDSpentShell",
						cos(pitch)*5,frandom(0.7,1.0),height-10-sin(pitch)*5,
						vel.x,vel.y,vel.z,
						0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
					);
				}else HDF.Give(self,slug?"HDSlugAmmo":"HDShellAmmo",2);
			}else{
				bool slug=invoker.weaponstatus[0]&WSTEF_SLUGS;
				if(invoker.wronghand){
					A_MuzzleClimb(frandom(0.4,0.8),frandom(0.4,1.4));
				}else A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
				A_TakeInventory(slug?"HDSlugAmmo":"HDShellAmmo",2,TIF_NOTAKEINFINITE);
				invoker.weaponstatus[WSTES_CHAMBER1]=2;
			}
		}
		#### I 2 offset(0,85) A_StartSound("weapons/WsteM5_load",8);
		#### I 2 offset(0,72);
		#### I 1 offset(0,65);
		#### I 1 offset(0,54);
		#### I 1 offset(0,46);
		#### I 1 offset(0,40);
		goto altholdaltend;
	altholdaltend:
		#### I 0 A_StartSound("weapons/WsteM5_click",8,CHANF_OVERLAP);
		#### HG 2{
			A_WeaponReady(WRF_NOFIRE);
			if(invoker.wronghand){
				A_MuzzleClimb(-frandom(0.2,0.5),frandom(0.5,1.0));
			}else A_MuzzleClimb(frandom(0.2,0.5),frandom(0.5,1.0));
		}
		#### F 2;
		#### A 8 ;//{
			//A_WeaponReady(WRF_NOFIRE);
		//}
		goto ready;
	altholdend:
		#### I 0 A_StartSound("weapons/WsteM5_flip",8,CHANF_OVERLAP);
		#### JKL 2 {
			if(invoker.wronghand){
				A_MuzzleClimb(frandom(0.4,0.6),-frandom(0.2,0.3));
			}else A_MuzzleClimb(-frandom(0.4,0.6),-frandom(0.2,0.3));
		}
		#### L 0 {
			//eject whatever is already loaded
			vector3 cockdir;double cp=cos(pitch);
			cockdir=(0,-cp*4,sin(pitch)*frandom(2,4));
			cockdir.xy=rotatevector(cockdir.xy,angle);
			int chm=invoker.weaponstatus[WSTES_CHAMBER1];
			bool slug=invoker.weaponstatus[0]&WSTEF_SLUGS;
			invoker.weaponstatus[WSTES_CHAMBER1]=0;
			if(chm>1){
				A_SpawnItemEx(slug?"HDFumblingSlug":"HDFumblingShell",
					cos(pitch)*5,frandom(-0.6,-0.8),height-12-sin(pitch)*5,
					vel.x-cockdir.x,vel.y-cockdir.y,vel.z+cockdir.z,
					0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
				);
				A_SpawnItemEx(slug?"HDFumblingSlug":"HDFumblingShell",
					cos(pitch)*5,frandom(0.6,0.8),height-12-sin(pitch)*5,
					vel.x+cockdir.x,vel.y+cockdir.y,vel.z+cockdir.z,
					0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
				);
			}
			else if(chm==1){
				A_SpawnItemEx(slug?"HDSpentSlug":"HDSpentShell",
					cos(pitch)*5,frandom(-0.6,-0.8),height-12-sin(pitch)*5,
					vel.x-cockdir.x,vel.y-cockdir.y,vel.z+cockdir.z,
					0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
				);
				A_SpawnItemEx(slug?"HDSpentSlug":"HDSpentShell",
					cos(pitch)*5,frandom(0.6,0.8),height-12-sin(pitch)*5,
					vel.x+cockdir.x,vel.y+cockdir.y,vel.z+cockdir.z,
					0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
				);
			}
		}
		#### L 0 {
			//and then load shells
			if(invoker.weaponstatus[WSTES_TUBE]>0){
				invoker.weaponstatus[WSTES_CHAMBER1]=2;
				invoker.weaponstatus[WSTES_TUBE]--;
			}
			//invoker.weaponstatus[WSTES_CHAMBER2]=2;
		}
		#### NO 2{
			if(invoker.wronghand){
				A_MuzzleClimb(-frandom(0.1,0.2),frandom(0.4,0.6));
			}else A_MuzzleClimb(frandom(0.1,0.2),frandom(0.4,0.6));
		}
		#### PQ 2{
			if(invoker.wronghand){
				A_MuzzleClimb(-frandom(0.2,0.4),frandom(0.2,0.3));
			}else A_MuzzleClimb(frandom(0.2,0.4),frandom(0.2,0.3));
		}
		#### A 1 {
			A_WeaponReady(WRF_NOFIRE);
		}
		goto ready;
	ready:
		WSTE A 0 A_CheckPistolHand(); //let the PostBeginPlay handle the presence of the relevant sprite
		#### A 1{
			int pff;
			bool paf=pressingaltfire();
			pff=PressingFire();
			bool ch1=invoker.weaponstatus[WSTES_CHAMBER1]==2;
			//bool ch2=invoker.weaponstatus[WSTES_CHAMBER2]==2;

			bool dbl=invoker.weaponstatus[0]&WSTEF_DOUBLE;
			if(ch1){
				if(pff>=1){
					A_Overlay(PSP_FLASH,"flashboth");
					return;
				}
				else if(pff&&dbl){
					setweaponstate("double");
					return;
				}
			}
			if(paf)setweaponstate("altfire");
			A_WeaponReady((WRF_ALL|WRF_NOFIRE)&~WRF_ALLOWUSER2);
		}
		#### A 0 A_WeaponReady();
		goto readyend;
	double:
		#### A 1 offset(0,34);
		#### A 1 offset(0,33);
		#### A 0 A_Overlay(PSP_FLASH,"flashboth");
		goto readyend;

	flasheither:
		TNT1 A 0 A_AlertMonsters();
		TNT1 A 0 setweaponstate("recoil");
		stop;
	flashboth:
		WSTE B 0 A_JumpIf(invoker.wronghand,"flashboth2");
		---- B 1 bright{
			A_Light2();
			HDFlashAlpha(128);
			A_StartSound("weapons/WsteM5_fire1",CHAN_WEAPON,CHANF_OVERLAP);
			A_StartSound("weapons/WsteM5_fire2",CHAN_WEAPON,CHANF_OVERLAP);
			A_ZoomRecoil(0.7);
			invoker.weaponstatus[WSTES_CHAMBER1]=1;
			//invoker.weaponstatus[WSTES_CHAMBER2]=1;

			invoker.Fire(self,0);
			invoker.Fire(self,1);
		}
		TNT1 A 1{
			A_Light0();
			double shotpower=invoker.shotpower;
			double mlt=(invoker.bplayingid?0.9:-0.9)*shotpower;
			double mlt2=-4.*shotpower;
			A_MuzzleClimb(mlt,mlt2,mlt,mlt2);
		}goto flasheither;
	flashboth2:
		WST2 B 0 {
			A_JumpIf(invoker.weaponstatus[WSTES_CHAMBER1]>2,"nope");
			//A_JumpIf(invoker.weaponstatus[WSTES_CHAMBER2]>2,"nope");
			}
		---- B 1 bright{
			A_Light2();
			HDFlashAlpha(128);
			A_StartSound("weapons/WsteM5_fire1",CHAN_WEAPON,CHANF_OVERLAP);
			A_StartSound("weapons/WsteM5_fire2",CHAN_WEAPON,CHANF_OVERLAP);
			A_ZoomRecoil(0.7);
			invoker.weaponstatus[WSTES_CHAMBER1]=1;
			//invoker.weaponstatus[WSTES_CHAMBER2]=1;

			invoker.Fire(self,0);
			invoker.Fire(self,1);
		}
		TNT1 A 1{
			A_Light0();
			double shotpower=invoker.shotpower;
			double mlt=(invoker.bplayingid?0.9:-0.9)*shotpower;
			double mlt2=-4.*shotpower;
			A_MuzzleClimb(mlt,mlt2,mlt,mlt2);
		}goto flasheither;
	recoil:
		#### CDE 1;
		goto ready;

	reload:
		#### F 1 offset(0,39);
		#### F 1 offset(0,42);
		#### G 2 offset(0,45);
		#### G 2 offset(0,47);
		#### H 3 offset(0,52){
			A_StartSound("weapons/WsteM5_open",8,CHANF_OVERLAP,0.9,pitch:0.95);
			if(invoker.wronghand){
				A_MuzzleClimb(frandom(0.4,0.8),frandom(0.4,1.4));
			}else A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
		}
		#### H 3 offset(0,60){
			//A_StartSound("weapons/rifleload",8,CHANF_OVERLAP);
			//A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
		}
		#### H 0 offset(0,60){
			int mg=invoker.weaponstatus[WSTES_TUBE];
			if(mg==3)setweaponstate("reloaddone");
		}
	loadhand:
		//#### I 10 offset(0,85);
		#### I 0 {
			bool slug=invoker.weaponstatus[0]&WSTEF_SLUGS;
			int amo=countinv(slug?"HDSlugAmmo":"HDShellAmmo");
			if(amo>=2)setweaponstate("loadhandloop");
		//A_JumpIfInventory("HDShellAmmo",2,"loadhandloop");
		}
		goto reloaddone;
	loadhandloop:
		#### I 1 offset(0,85){
			bool slug=invoker.weaponstatus[0]&WSTEF_SLUGS;
			int hnd=min(
				countinv(slug?"HDSlugAmmo":"HDShellAmmo"),2,
				4-invoker.weaponstatus[WSTES_TUBE]
			);
			if(hnd<2){
				setweaponstate("reloaddone");
				return;
			}else{
				//A_TakeInventory("HDShellAmmo",hnd,TIF_NOTAKEINFINITE);I need to figure out EmptyHand(), moving the takeinv to a different line for the time being
				invoker.weaponstatus[WSTES_HAND]=hnd;
			}
		}
		#### I 2 offset(0,88){
			A_StartSound("weapons/pocket",9);
			A_MuzzleClimb(
				frandom(0.1,0.15),frandom(0.2,0.4),
				frandom(0.2,0.25),frandom(0.3,0.4),
				frandom(0.1,0.35),frandom(0.3,0.4),
				frandom(0.1,0.15),frandom(0.2,0.4)
			);
		}
		#### I 2 offset(0,87){
			A_StartSound("weapons/pocket",9);
		}
		#### I 4 offset(0,85);
	loadone:
		#### I 4 offset(0,85) A_JumpIf(invoker.weaponstatus[WSTES_HAND]<1,"loadhandnext");
		#### I 3 offset(0,80){
			bool slug=invoker.weaponstatus[0]&WSTEF_SLUGS;
			A_TakeInventory(slug?"HDSlugAmmo":"HDShellAmmo",2,TIF_NOTAKEINFINITE);
			invoker.weaponstatus[WSTES_HAND]=-2;
			invoker.weaponstatus[WSTES_TUBE]++;
			A_StartSound("weapons/WsteM5_load",8);
		}
		#### I 5 offset(0,80) A_StartSound("weapons/WsteM5_load",8);
		#### I 4 offset(0,80);
		loop;
	loadhandnext:
        #### I 6 offset(0,82)
        {
            if(!PressingReload()&&
				!PressingFire()&&
				!PressingAltFire()&&
				!PressingZoom()||
				!invoker.weaponstatus[0]&WSTEF_SLUGS&!countinv("HDShellAmmo")||
				invoker.weaponstatus[0]&WSTEF_SLUGS&!countinv("HDSlugAmmo")) //this is getting out of hand
				setweaponstate("reloaddone");
            else A_StartSound("weapons/pocket",9);
        }
        goto loadhandloop;
	reloaddone:
		#### H 3 offset(0,55);
		#### G 1 offset(0,47);
		#### G 1 EmptyHand(careful:true);
		#### F 2 offset(0,39);
		goto nope;
	unload:
		#### F 1 offset(0,39);
		#### F 1 offset(0,42);
		#### G 2 offset(0,45);
		#### G 1 offset(0,47);
		#### G 1 EmptyHand(careful:true);
		#### H 3 offset(0,52){
			if(invoker.wronghand){
				A_MuzzleClimb(frandom(0.4,0.8),frandom(0.4,1.4));
			}else A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
			A_StartSound("weapons/WsteM5_open",8);
		}
		#### H 4 offset (0,60){
			//A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
			//A_StartSound("weapons/WsteM5_load",8);
		}
	unloadloop:
		#### I 10 offset(0,85);
		#### I 3 offset(0,85){
			if(invoker.weaponstatus[WSTES_TUBE]<1)setweaponstate("unloaddone");
			else{
				bool slug=invoker.weaponstatus[0]&WSTEF_SLUGS;
				A_StartSound("weapons/WsteM5_load",8);
				invoker.weaponstatus[WSTES_TUBE]--;
				if(A_JumpIfInventory(slug?"HDSlugAmmo":"HDShellAmmo",0,"null")){
					A_SpawnItemEx(
						slug?"HDFumblingSlug":"HDFumblingShell",cos(pitch)*8,0,height-9-sin(pitch)*8,
						cos(pitch)*cos(angle-40)*1+vel.x,
						cos(pitch)*sin(angle-40)*1+vel.y,
						-sin(pitch)*1+vel.z,
						0,SXF_ABSOLUTEMOMENTUM|
						SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
					);
					A_SpawnItemEx(
						slug?"HDFumblingSlug":"HDFumblingShell",cos(pitch)*8,0,height-9-sin(pitch)*8,
						cos(pitch)*cos(angle-40)*1+vel.x,
						cos(pitch)*sin(angle-40)*1+vel.y,
						-sin(pitch)*1+vel.z,
						0,SXF_ABSOLUTEMOMENTUM|
						SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
					);
				}else A_GiveInventory(slug?"HDSlugAmmo":"HDShellAmmo",2);
			}
		}
		#### I 5 offset(0,85) A_StartSound("weapons/WsteM5_load",8);
		#### I 4 offset(0,87) A_StartSound("weapons/pocket",8,CHANF_OVERLAP);
		#### I 3 offset(0,85);
		#### I 0{
			if(
				PressingReload()||
				PressingFire()||
				PressingAltFire()||
				PressingZoom()
			)setweaponstate("unloaddone");
		}loop;
	unloaddone:
		#### H 2 offset(0,55);
		#### G 3 offset(0,47);
		#### F 1 offset(0,40) A_StartSound("weapons/WsteM5_open",8);
		#### A 1 offset(0,36);
		#### A 1 offset(0,34);
		goto ready;

	altreload:
	swappistols:
		---- A 0 A_SwapHandguns();
		---- A 0{
			bool id=(Wads.CheckNumForName("id",0)!=-1);
			bool offhand=invoker.wronghand;
			bool lefthanded=(id!=offhand);
			if(lefthanded){
				A_Overlay(1025,"raiseleft");
				A_Overlay(1026,"lowerright");
			}else{
				A_Overlay(1025,"raiseright");
				A_Overlay(1026,"lowerleft");
			}
		}
		TNT1 A 5;
		WSTE A 0 A_CheckPistolHand();
		goto nope;
	lowerleft:
		WSTE A 0 A_JumpIf(Wads.CheckNumForName("id",0)!=-1,2);
		WST2 A 0;
		#### H 1 offset(-6,38);
		#### H 1 offset(-12,48);
		#### H 1 offset(-20,60);
		#### H 1 offset(-34,76);
		#### H 1 offset(-50,86);
		stop;
	lowerright:
		WST2 A 0 A_JumpIf(Wads.CheckNumForName("id",0)!=-1,2);
		WSTE A 0;
		#### H 1 offset(6,38);
		#### H 1 offset(12,48);
		#### H 1 offset(20,60);
		#### H 1 offset(34,76);
		#### H 1 offset(50,86);
		stop;
	raiseleft:
		WSTE A 0 A_JumpIf(Wads.CheckNumForName("id",0)!=-1,2);
		WST2 A 0;
		#### H 1 offset(-50,86);
		#### H 1 offset(-34,76);
		#### H 1 offset(-20,60);
		#### H 1 offset(-12,48);
		#### H 1 offset(-6,38);
		stop;
	raiseright:
		WST2 A 0 A_JumpIf(Wads.CheckNumForName("id",0)!=-1,2);
		WSTE A 0;
		#### H 1 offset(50,86);
		#### H 1 offset(34,76);
		#### H 1 offset(20,60);
		#### H 1 offset(12,48);
		#### H 1 offset(6,38);
		stop;
	whyareyousmiling:
		#### H 1 offset(0,48);
		#### H 1 offset(0,60);
		#### H 1 offset(0,76);
		TNT1 A 7;
		WSTE A 0{
			invoker.wronghand=!invoker.wronghand;
			A_CheckPistolHand();
		}
		#### H 1 offset(0,76);
		#### H 1 offset(0,60);
		#### H 1 offset(0,48);
		goto nope;

	spawn:
		WSTE X -1;
		stop;
	}
	override void InitializeWepStats(bool idfa){
		weaponstatus[WSTES_CHAMBER1]=2;
		weaponstatus[WSTES_TUBE]=3;
	}
	override void LoadoutConfigure(string input){
		int slug=getloadoutvar(input,"slug",1);
		if (slug>0)
			weaponstatus[0]|=WSTEF_SLUGS;
		else
			weaponstatus[0]&=~WSTEF_SLUGS;
	}
}
enum wstem5status{
	WSTEF_JUSTUNLOAD=1,
	WSTEF_DOUBLE=2,
	WSTEF_SLUGS=4,
	//WSTEF_FROMPOCKETS=4,

	WSTES_CHAMBER1=1,
	//WSTES_CHAMBER2=2,
	WSTES_HAND=2,
	WSTES_TUBE=3,
	WSTES_HEAT1=4,
	WSTES_HEAT2=5,
};


class WAN_WSTEM5Spawner:IdleDummy{
	states{
	spawn:
		TNT1 A 0 nodelay{
			let wst=WAN_WSTEM5(spawn("WAN_WSTEM5",pos,ALLOW_REPLACE));
			if(!wst)return;
			HDF.TransferSpecials(self,wst);
			if(!random(0,2)){
				wst.weaponstatus[0]|=WSTEF_SLUGS;
				spawn("SlugBoxPickup",pos+(-3,0,0),ALLOW_REPLACE);
			}
			else
			spawn("ShellBoxPickup",pos+(-3,0,0),ALLOW_REPLACE);
		}stop;
	}
}
