//One majorly explosive cigar
class WAN_SPNKR:HDWeapon{
	string ammotype;
	string setrocket;
	default{
		+weapon.explosive
		weapon.selectionorder 92;
		weapon.slotnumber 5;
		weapon.slotpriority 1.1;
		scale 0.6;
		inventory.pickupsound "weapons/WsteM5_pickup";
		//inventory.pickupmessage "You got the SPNKR-XP launcher!";
		obituary "$OB_ROCKET";
		hdweapon.barrelsize 36,3.1,5;
		hdweapon.refid 'spk';
		tag "$WAN_TAG_SPNKR";
		
		hdweapon.loadoutcodes "
			\cuchm - 0-3, what rocket type is loaded
			\cuscope - 0/1, enable/disable scope
			\cureflex - 0/1, enable/disable dot sight";
	}
	
	override string pickupmessage() {return Stringtable.Localize("$WAN_PICK_SPNKR");}
	
	action void GetAmmoType(int chm){
		switch(chm){
			case 1:
				invoker.ammotype="HeatAmmo";
				invoker.setrocket="HDHeat";
				break;
			case 2:
				invoker.ammotype="WAN_ThuRktAmmo";
				invoker.setrocket="WAN_ThuRkt";
				break;
			case 3:
				invoker.ammotype="WAN_TortRktAmmo";
				invoker.setrocket="WAN_TortRkt";
				break;
			default://default to heat just in case
				invoker.ammotype="HeatAmmo";
				invoker.setrocket="HDHeat";
				break;
		}
	}
	action void UnloadAmmo(){
		if(!pressingunload()&&!PressingReload()
			||A_JumpIfInventory(invoker.ammotype,0,"null")
		){
			A_SpawnItemEx(
				invoker.ammotype,10,0,height-16,vel.x,vel.y,vel.z+2,
				0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION
			);
		}else{
				A_StartSound("weapons/pocket",9);
				A_GiveInventory(invoker.ammotype,1);
				A_SetTics(8);
		}
	}
	action void A_FireCustomRocket(int chm){//maybe add another variable to adjust the rocket offset when firing depending on chamber
		rocketgrenade rkt;//vanilla heat
		WAN_CustomRocket wkt;//custom rockets
		GetAmmoType(chm);
		class<actor> rrr;
		if(chm==1){//fire a vanilla heat rocket instead of any custom ones
			rrr="HDHEAT";
			A_ChangeVelocity(cos(pitch),0,sin(pitch),CVF_RELATIVE);
			vector3 gpos=pos+gunpos((0,0,-getdefaultbytype(rrr).height*0.6));
			rkt=rocketgrenade(spawn(rrr,gpos,ALLOW_REPLACE));
			invoker.weaponstatus[SPNKRS_SMOKE]+=50;
			rkt.target=self;rkt.master=self;

			let hdp=hdplayerpawn(self);
			if(hdp){
				rkt.angle=hdp.gunangle;
				rkt.pitch=hdp.gunpitch;
			}else{
				rkt.angle=angle;
				rkt.pitch=pitch;
			}

			rkt.primed=false;
			rkt.isrocket=true;
			if(!(player.cmd.buttons&BT_ZOOM))invoker.airburst=0;
		}else if(chm>1){//don't just assume the chamber is loaded even at this point
			rrr=invoker.setrocket;
			A_ChangeVelocity(cos(pitch),0,sin(pitch),CVF_RELATIVE);
			vector3 gpos=pos+gunpos((0,0,-getdefaultbytype(rrr).height*0.6));
			wkt=WAN_CustomRocket(spawn(rrr,gpos,ALLOW_REPLACE));
			invoker.weaponstatus[SPNKRS_SMOKE]+=50;
			wkt.target=self;wkt.master=self;

			let hdp=hdplayerpawn(self);
			if(hdp){
				wkt.angle=hdp.gunangle;
				wkt.pitch=hdp.gunpitch;
			}else{
				wkt.angle=angle;
				wkt.pitch=pitch;
			}
			if(!(player.cmd.buttons&BT_ZOOM))invoker.airburst=0;
		}else setweaponstate("nope");
		A_StartSound("weapons/spnkr_fire",CHAN_WEAPON,CHANF_OVERLAP);
	}
	override bool AddSpareWeapon(actor newowner){return AddSpareWeaponRegular(newowner);}
	override hdweapon GetSpareWeapon(actor newowner,bool reverse,bool doselect){return GetSpareWeaponRegular(newowner,reverse,doselect);}
	override void tick(){
		super.tick();
		drainheat(SPNKRS_SMOKE,12);
	}
	override double gunmass(){
		int mass = 8;//total mass when loaded should be slightly less than a magged launcher that's loaded
		if(weaponstatus[SPNKRS_CHAMBER1]>0)mass+=2;
		if(weaponstatus[SPNKRS_CHAMBER2]>0)mass+=2;
		return mass;
	}
	override double weaponbulk(){
		double blx=110;//weigh somewhere between a nomag launcher and magged launcher

		int chmb1=weaponstatus[SPNKRS_CHAMBER1];
		int chmb2=weaponstatus[SPNKRS_CHAMBER2];
		if(chmb1>0)blx+=ENC_HEATROCKETLOADED;
		if(chmb2>0)blx+=ENC_HEATROCKETLOADED;
		return blx;
	}
	override void beginplay(){
		super.beginplay();
	}
	override string,double getpickupsprite(){
		return "SPKRA0",1.;
	}
	override void DrawHUDStuff(HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl){
		if(sb.hudlevel==1){
			switch(hdw.weaponstatus[SPNKRS_AMMOTYPE]){//switch statement for future ammo types
				case 1:
					sb.drawimage("TRTRB0",(-66,-8),sb.DI_SCREEN_CENTER_BOTTOM,alpha:0.5,scale:(0.4,0.4));
					sb.drawimage("THRKB0",(-50,-8),sb.DI_SCREEN_CENTER_BOTTOM,alpha:0.5,scale:(0.4,0.4));
					sb.drawimage("ROCKA0",(-58,-4),sb.DI_SCREEN_CENTER_BOTTOM,scale:(0.6,0.6));
					sb.drawnum(hpl.countinv("HEATAmmo"),-54,-8,sb.DI_SCREEN_CENTER_BOTTOM);
					break;
				case 2:
					sb.drawimage("ROCKA0",(-66,-8),sb.DI_SCREEN_CENTER_BOTTOM,alpha:0.5,scale:(0.4,0.4));
					sb.drawimage("TRTRB0",(-50,-8),sb.DI_SCREEN_CENTER_BOTTOM,alpha:0.5,scale:(0.4,0.4));
					sb.drawimage("THRKB0",(-58,-4),sb.DI_SCREEN_CENTER_BOTTOM,scale:(0.6,0.6));
					sb.drawnum(hpl.countinv("WAN_ThuRktAmmo"),-54,-8,sb.DI_SCREEN_CENTER_BOTTOM);
					break;
				case 3:
					sb.drawimage("THRKB0",(-66,-8),sb.DI_SCREEN_CENTER_BOTTOM,alpha:0.5,scale:(0.4,0.4));
					sb.drawimage("ROCKA0",(-50,-8),sb.DI_SCREEN_CENTER_BOTTOM,alpha:0.5,scale:(0.4,0.4));
					sb.drawimage("TRTRB0",(-58,-4),sb.DI_SCREEN_CENTER_BOTTOM,scale:(0.6,0.6));
					sb.drawnum(hpl.countinv("WAN_TortRktAmmo"),-54,-8,sb.DI_SCREEN_CENTER_BOTTOM);
					break;
				default:
					break;
			}
		}
		if(hdw.weaponstatus[0]&SPNKRF_DOUBLE){
			sb.drawimage("STBURAUT",(-28,-26),sb.DI_SCREEN_CENTER_BOTTOM);
		}
		color thunderCol = color(255,60,70,210);
		color heatCol = color(255,123,116,112);
		color tortoiseCol = color(255,180,40,32);
		color chmOne;
		color chmTwo;
		switch(hdw.weaponstatus[SPNKRS_CHAMBER1]){
			case 1:
				chmOne = heatCol;
				break;
			case 2:
				chmOne = thunderCol;
				break;
			case 3:
				chmOne = tortoiseCol;
				break;
			default:
				chmOne = heatCol;//just in case
				break;
		}
		switch(hdw.weaponstatus[SPNKRS_CHAMBER2]){
			case 1:
				chmTwo = heatCol;
				break;
			case 2:
				chmTwo = thunderCol;
				break;
			case 3:
				chmTwo = tortoiseCol;
				break;
			default:
				chmTwo = heatCol;//just in case
				break;
		}
		if(hdw.weaponstatus[SPNKRS_CHAMBER1]>0){
			sb.Fill(chmOne,-35,-18,4,12,sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_ITEM_RIGHT);
			sb.Fill(chmOne,-34.5,-22,3,4,sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_ITEM_RIGHT);
			sb.Fill(chmOne,-34,-24,2,2,sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_ITEM_RIGHT);
			sb.Fill(chmOne,-33.5,-26,1,2,sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_ITEM_RIGHT);
			sb.Fill(chmOne,-36.5,-12,7,5,sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_ITEM_RIGHT);
			sb.Fill(chmOne,-35.5,-13,5,1,sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_ITEM_RIGHT);
		}
		if(hdw.weaponstatus[SPNKRS_CHAMBER2]>0){
			sb.Fill(chmTwo,-26,-18,4,12,sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_ITEM_RIGHT);
			sb.Fill(chmTwo,-25.5,-22,3,4,sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_ITEM_RIGHT);
			sb.Fill(chmTwo,-25,-24,2,2,sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_ITEM_RIGHT);
			sb.Fill(chmTwo,-24.5,-26,1,2,sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_ITEM_RIGHT);
			sb.Fill(chmTwo,-27.5,-12,7,5,sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_ITEM_RIGHT);
			sb.Fill(chmTwo,-26.5,-13,5,1,sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_ITEM_RIGHT);
		}
		//use a translatable graphic instead?
	}
	override string gethelptext(){
		return
		WEPHELP_FIRE.."  Shoot Left\n"
		..WEPHELP_ALTFIRE.."  Shoot Right\n"
		..WEPHELP_RELOAD.."  Reload \(Hold "..WEPHELP_FIREMODE.." to reverse order\)\n"
		..WEPHELP_ALTRELOAD.."  Change rocket type\n"
		..WEPHELP_FIREMODE.."  Hold to force double shot\n"
		..WEPHELP_UNLOAD.."  Reload \(Hold "..WEPHELP_FIREMODE.." to reverse order\)"
		;
	}
	override void DrawSightPicture(
		HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl,
		bool sightbob,vector2 bob,double fov,bool scopeview,actor hpc,string whichdot
	){
		vector2 bobb=bob*1.1;
		bobb.y=clamp(bobb.y,-8,8);
		if(weaponstatus[0]&SPNKRF_DTSIT){
			bobb=bob*1.18;
			double dotoff=max(abs(bob.x),abs(bob.y));
			if(dotoff<30){
				string whichdot=sb.ChooseReflexReticle(hdw.weaponstatus[SPNKRS_DOT]);
				sb.drawimage(
					whichdot,(0,0)+bobb,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER,
					alpha:0.8-dotoff*0.01,scale:(1.1,1.1),
					col:0xFF000000|sb.crosshaircolor.GetInt()
				);
			}
			sb.drawimage(
				"rlrearsight",(0,0)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER
			);
		}else{
			bobb=bob*1.1;
			int cx,cy,cw,ch;
			[cx,cy,cw,ch]=Screen.GetClipRect();
			sb.SetClipRect(
				-16+bob.x,-32+bob.y,32,38,
				sb.DI_SCREEN_CENTER
			);
			sb.SetClipRect(cx,cy,cw,ch);
			sb.drawimage(
				"ma75bsit",(0,2)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
				scale:(1.5,1.3)
			);
		}
		if(scopeview&&(weaponstatus[0]&SPNKRF_SCOPE)){
			double degree=4.;
			double deg=1/degree;
			int scaledyoffset=40;
			int scaledwidth=56;
			int cx,cy,cw,ch;
			[cx,cy,cw,ch]=screen.GetClipRect();
			sb.SetClipRect(
				bob.x-(scaledwidth>>1),bob.y+scaledyoffset-(scaledwidth>>1),
				scaledwidth,scaledwidth,
				sb.DI_SCREEN_CENTER
			);
			//the new circular view code that doesn't work with LZDoom 3.87c
			sb.fill(color(255,0,0,0),
				bob.x-27,scaledyoffset+bob.y-27,
				54,54,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER
			);
			texman.setcameratotexture(hpc,"HDXCAM_RLAUN",degree);
			let cam  = texman.CheckForTexture("HDXCAM_RLAUN",TexMan.Type_Any);
			double camSize = texman.GetSize(cam);
			sb.DrawCircle(cam,(0,scaledyoffset)+bob*5,.085,usePixelRatio:true);
			screen.SetClipRect(cx,cy,cw,ch);
			sb.drawimage(
				"rlret",(0,scaledyoffset)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER,
				scale:(0.82,0.82)
			);
			sb.drawimage(
				"rlscop",(0,scaledyoffset)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER,
				scale:(0.82,0.82)
			);
		}
	}
	override void SetReflexReticle(int which){weaponstatus[SPNKRS_DOT]=which;}
	override void DropOneAmmo(int amt){
		if(owner){
			amt=clamp(amt,1,1);
			GetAmmoType(weaponstatus[SPNKRS_AMMOTYPE]);//only drop rockets you have currently selected
			owner.A_DropInventory(ammotype,1);
		}
	}
	override void ForceBasicAmmo(){
		owner.A_SetInventory("HEATAmmo",1);
	}
	states{
	select0:
		SPNK A 0 A_CheckDefaultReflexReticle(SPNKRS_DOT);
		goto select0bfg;
	deselect0:
		SPNK A 0;
		goto deselect0bfg;
	fire:
	altfire:
		#### A 0 A_ClearRefire();
	ready:
		SPNK A 0;
		#### A 2 {
			if(PressingFireMode()){
				invoker.weaponstatus[0]|=SPNKRF_DOUBLE;
			}else invoker.weaponstatus[0]&=~SPNKRF_DOUBLE;
			
			int pff;
			pff=PressingFire();
			if(PressingAltfire())pff|=2;
			
			bool ch1;
			bool ch2;
			if(invoker.weaponstatus[SPNKRS_CHAMBER1]>0)ch1=true;
			else ch1=false;
			if(invoker.weaponstatus[SPNKRS_CHAMBER2]>0)ch2=true;
			else ch2=false;
			
			bool dbl=invoker.weaponstatus[0]&SPNKRF_DOUBLE;
			//Comment out most uses of overlays in the firing states
			//Was causing read from zero aborts with the rocket firing code and I don't even want to try fixing that
			if(ch1&&ch2){
				if(pff==3){
					setweaponstate("flashboth");//A_Overlay(PSP_FLASH,"flashboth");
					return;
				}
				else if(pff&&dbl){
					setweaponstate("double");
					return;
				}
			}else if(pff&&dbl){
				if(invoker.weaponstatus[SPNKRS_CHAMBER1]>0)setweaponstate("flashleft");//A_Overlay(11,"flashleft");
				if(invoker.weaponstatus[SPNKRS_CHAMBER2]>0)setweaponstate("flashright");//A_Overlay(12,"flashright");
			}
			if(ch1&&pff%2)setweaponstate("flashleft");//;A_Overlay(11,"flashleft");
			else if(ch2&&pff>1)setweaponstate("flashright");//A_Overlay(12,"flashright");
			else A_WeaponReady((WRF_ALL|WRF_NOFIRE)&~WRF_ALLOWUSER2);
		}
		#### A 0 A_WeaponReady();
		goto readyend;
	double:
		#### A 1 offset(0,34);
		#### A 1 offset(0,33);
		#### A 0 setweaponstate("flashboth");//A_Overlay(PSP_FLASH,"flashboth");
		//goto readyend;
		stop;
	flashleft:
		SPNK A 1{
			//if(!invoker.weaponstatus[SPNKRS_CHAMBER1]>0)setweaponstate("nope");
			A_FireCustomRocket(invoker.weaponstatus[SPNKRS_CHAMBER1]);
			invoker.weaponstatus[SPNKRS_CHAMBER1]=0;
		}
		#### A 1;
		goto flasheither;
	flashright:
		SPNK A 1{
			A_FireCustomRocket(invoker.weaponstatus[SPNKRS_CHAMBER2]);
			invoker.weaponstatus[SPNKRS_CHAMBER2]=0;
		}
		#### A 1;
		goto flasheither;
	flasheither:
		#### A 0 A_AlertMonsters();
		#### A 0 setweaponstate("recoil");
		stop;
	flashboth:
		SPNK A 2{
			A_FireCustomRocket(invoker.weaponstatus[SPNKRS_CHAMBER1]);
			invoker.weaponstatus[SPNKRS_CHAMBER1]=0;
		}
		SPNK A 1{
			A_FireCustomRocket(invoker.weaponstatus[SPNKRS_CHAMBER2]);
			invoker.weaponstatus[SPNKRS_CHAMBER2]=0;
		}
		#### A 2{
			A_MuzzleClimb(
				0,0,
				frandom(1.2,1.7),-frandom(2,2.5),
				frandom(1.,1.2),-frandom(2.5,3),
				frandom(0.6,0.8),-frandom(2,3)
			);
			A_ChangeVelocity(cos(pitch)*-3,0,sin(pitch)*3,CVF_RELATIVE);
			if(self is "hdplayerpawn")hdplayerpawn(self).stunned+=10;
			damagemobj(invoker,self,10,"bashing");
		}goto flasheither;
	recoil:
		#### A 2 bright{
			A_ZoomRecoil(0.7);
			if(gunbraced()){
				hdplayerpawn(self).gunbraced=false;
				A_MuzzleClimb(
					0,0,
					frandom(1,1.2),-frandom(1,1.5),
					frandom(0.7,0.9),-frandom(1.5,2),
					-frandom(0.8,1.),frandom(2,3)
				);
				A_ChangeVelocity(cos(pitch)*-1,0,sin(pitch)*1,CVF_RELATIVE);
			}else{
				A_MuzzleClimb(
					0,0,
					frandom(1.2,1.7),-frandom(2,2.5),
					frandom(1.,1.2),-frandom(2.5,3),
					frandom(0.6,0.8),-frandom(2,3)
				);
				A_ChangeVelocity(cos(pitch)*-3,0,sin(pitch)*3,CVF_RELATIVE);
				if(self is "hdplayerpawn")hdplayerpawn(self).stunned+=10;
			}
			HDFlashAlpha(128);
			A_Light1();
		}
		#### A 2 bright A_Light2();
		#### A 2 bright A_Light1();
		#### A 1 bright A_Light0();
		goto ready;
	reload:
		#### A 0 {
			switch(invoker.weaponstatus[SPNKRS_AMMOTYPE]){
				case 1:
					invoker.ammotype="HEATAmmo";
					break;
				case 2:
					invoker.ammotype="WAN_ThuRktAmmo";
					break;
				case 3:
					invoker.ammotype="WAN_TortRktAmmo";
					break;
				default://default to heat if anything goes wrong
					invoker.ammotype="HEATAmmo";
					break;
			}
			if(
				(invoker.weaponstatus[SPNKRS_CHAMBER1]>0&&invoker.weaponstatus[SPNKRS_CHAMBER2]>0)
				||!countinv(invoker.ammotype)
			)setweaponstate("nope");
		}
		#### A 1 offset(-2,34);
		#### A 1 offset(-4,36) A_MuzzleClimb(frandom(1.2,2.4),frandom(1.2,2.4));
		#### A 1 offset(-10,38);
		#### A 4 offset(-12,40){
			A_MuzzleClimb(frandom(1.2,2.4),frandom(1.2,2.4));
			A_StartSound("weapons/spnkr_open",8);
		}
		#### A 10 offset(-11,38) A_StartSound("weapons/spnkr_open2",8);
	reload2:
		#### A 0 A_JumpIf(
			(invoker.weaponstatus[SPNKRS_CHAMBER1]>0&&invoker.weaponstatus[SPNKRS_CHAMBER2]>0)
			||!countinv(invoker.ammotype),
			"reloadend"
		);
		#### A 9 offset(-10,38) A_StartSound("weapons/pocket",9);
		#### A 8 offset(-12,40) A_StartSound("weapons/spnkr_reload",8);
		#### A 6 offset(-10,38){
			if(!countinv(invoker.ammotype))return;
			A_TakeInventory(invoker.ammotype,1,TIF_NOTAKEINFINITE);
			if(!pressingfiremode()){
				if(invoker.weaponstatus[SPNKRS_CHAMBER1]<1)
					invoker.weaponstatus[SPNKRS_CHAMBER1]=invoker.weaponstatus[SPNKRS_AMMOTYPE];
				else if(invoker.weaponstatus[SPNKRS_CHAMBER2]<1)
					invoker.weaponstatus[SPNKRS_CHAMBER2]=invoker.weaponstatus[SPNKRS_AMMOTYPE];
			}else{
				if(invoker.weaponstatus[SPNKRS_CHAMBER2]<1)
					invoker.weaponstatus[SPNKRS_CHAMBER2]=invoker.weaponstatus[SPNKRS_AMMOTYPE];
				else if(invoker.weaponstatus[SPNKRS_CHAMBER1]<1)
					invoker.weaponstatus[SPNKRS_CHAMBER1]=invoker.weaponstatus[SPNKRS_AMMOTYPE];
			}
		}
		#### A 5 offset(-10,34) A_JumpIf(!pressingreload(),"reloadend");
		#### A 4 offset(-11,38) A_StartSound("weapons/pocket",9);
		#### A 5 offset(-10,37);
		loop;
	reloadend:
		#### A 5 offset(-10,36) A_StartSound("weapons/spnkr_open2",8);
		#### A 1 offset(-8,38) A_StartSound("weapons/spnkr_open",8,CHANF_OVERLAP);
		#### A 1 offset(-4,36);
		#### A 1 offset(-2,34);
		goto nope;
	unload:
		#### A 0 {
			switch(invoker.weaponstatus[SPNKRS_AMMOTYPE]){
				case 1:
					invoker.ammotype="HEATAmmo";
					break;
				case 2:
					invoker.ammotype="WAN_ThuRktAmmo";
					break;
				case 3:
					invoker.ammotype="WAN_TortRktAmmo";
					break;
				default://default to heat if anything goes wrong
					invoker.ammotype="HEATAmmo";
					break;
			}
			if(
				(invoker.weaponstatus[SPNKRS_CHAMBER1]<1&&invoker.weaponstatus[SPNKRS_CHAMBER2]<1)
			)setweaponstate("nope");
		}
		#### A 1 offset(-2,34);
		#### A 1 offset(-4,36) A_MuzzleClimb(frandom(1.2,2.4),frandom(1.2,2.4));
		#### A 1 offset(-10,38);
		#### A 4 offset(-12,40){
			A_MuzzleClimb(frandom(1.2,2.4),frandom(1.2,2.4));
			A_StartSound("weapons/spnkr_open",8);
		}
		#### A 2 offset(-11,38) A_StartSound("weapons/spnkr_open2",8);
	unload2:
		#### A 0 A_JumpIf(
			(invoker.weaponstatus[SPNKRS_CHAMBER1]<1&&invoker.weaponstatus[SPNKRS_CHAMBER2]<1),
			"unloadend"
		);
		#### A 10 offset(-12,40) A_StartSound("weapons/spnkr_reload",8);
		#### A 9 offset(-10,38){
			if(!pressingfiremode()){
				if(invoker.weaponstatus[SPNKRS_CHAMBER1]>0){
					GetAmmoType(invoker.weaponstatus[SPNKRS_CHAMBER1]);
					invoker.weaponstatus[SPNKRS_CHAMBER1]=0;
					UnloadAmmo();
				}else if(invoker.weaponstatus[SPNKRS_CHAMBER2]>0){
					GetAmmoType(invoker.weaponstatus[SPNKRS_CHAMBER2]);
					invoker.weaponstatus[SPNKRS_CHAMBER2]=0;
					UnloadAmmo();
				}
			}else{
				if(invoker.weaponstatus[SPNKRS_CHAMBER2]>0){
					GetAmmoType(invoker.weaponstatus[SPNKRS_CHAMBER2]);
					invoker.weaponstatus[SPNKRS_CHAMBER2]=0;
					UnloadAmmo();
				}else if(invoker.weaponstatus[SPNKRS_CHAMBER1]>0){
					GetAmmoType(invoker.weaponstatus[SPNKRS_CHAMBER1]);
					invoker.weaponstatus[SPNKRS_CHAMBER1]=0;
					UnloadAmmo();
				}
			}
		}
		#### A 7 offset(-10,36) A_StartSound("weapons/spnkr_open2",8);
		#### A 5 offset(-10,36);
		#### A 2 A_JumpIf(!pressingunload(),"unloadend");
		#### A 3;
		loop;
	unloadend:
		#### A 2 offset(-8,38);
		#### A 2 offset(-4,36);
		#### A 1 offset(-2,34);
		goto nope;
	
	altreload:
		#### A 1{
			int maxtype=3;
			int type=invoker.weaponstatus[SPNKRS_AMMOTYPE];
			if(!pressingzoom()){
				if(type>=maxtype)invoker.weaponstatus[SPNKRS_AMMOTYPE]=1;
				else invoker.weaponstatus[SPNKRS_AMMOTYPE]++;
			}else{
				if(type<=1)invoker.weaponstatus[SPNKRS_AMMOTYPE]=maxtype;
				else invoker.weaponstatus[SPNKRS_AMMOTYPE]--;
			}
		}goto nope;
	spawn:
		SPKR A -1;
		stop;
	}
	override void InitializeWepStats(bool idfa){
		weaponstatus[SPNKRS_CHAMBER1]=1;
		weaponstatus[SPNKRS_CHAMBER2]=1;
		weaponstatus[SPNKRS_AMMOTYPE]=1;
		if(!idfa&&!owner){//wild spnkrs will never have dot sights or scopes but will have random rockets
			int rndrkt = random(1,3);
			weaponstatus[SPNKRS_CHAMBER1]=rndrkt;
			weaponstatus[SPNKRS_CHAMBER2]=rndrkt;
		}
	}
	override void loadoutconfigure(string input){
		int chm=getloadoutvar(input,"chm",3);
		if (chm>=0){
			weaponstatus[SPNKRS_CHAMBER1]=chm;
			weaponstatus[SPNKRS_CHAMBER2]=chm;
		}else{
			weaponstatus[SPNKRS_CHAMBER1]=1;
			weaponstatus[SPNKRS_CHAMBER2]=1;
		}
		
		int scope=getloadoutvar(input,"scope",1);
		if(scope>0)weaponstatus[0]|=SPNKRF_SCOPE;
		else weaponstatus[0]&=~SPNKRF_SCOPE;
		
		int xhdot=getloadoutvar(input,"dot",3);
		int reflex=getloadoutvar(input,"reflex",1);

		if(reflex>0){
			weaponstatus[0]|=SPNKRF_DTSIT;
			if(xhdot>=0)weaponstatus[SPNKRS_DOT]=xhdot;
		}else weaponstatus[0]&=~SPNKRF_DTSIT;
	}
}
enum spnkrstatus{
	SPNKRF_DOUBLE=1,
	SPNKRF_SCOPE=2,
	SPNKRF_DTSIT=4,

	SPNKRS_STATUS=0,
	SPNKRS_CHAMBER1=1,
	SPNKRS_CHAMBER2=2,
	SPNKRS_AMMOTYPE=3,
	SPNKRS_SMOKE=4,
	SPNKRS_DOT=5,
};

class WAN_CustomRocketSpawner:HDInvRandomSpawner{
	default{
		dropitem "Wan_ThuRKTAmmo",256,1;
		dropitem "Wan_TortRKTAmmo",256,1;
	}
}

class WAN_CustomRocketPickup:IdleDummy{
	states{
	spawn:
		TNT1 A 0 nodelay{
			A_SpawnItemEx("WAN_CustomRocketSpawner",-4,0,flags:SXF_NOCHECKPOSITION);
			A_SpawnItemEx("WAN_CustomRocketSpawner",0,0,flags:SXF_NOCHECKPOSITION);
			A_SpawnItemEx("WAN_CustomRocketSpawner",4,0,flags:SXF_NOCHECKPOSITION);
		}stop;
	}
}
