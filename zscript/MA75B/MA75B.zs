// ------------------------------------------------------------
// MA-75B Battle Rifle with integral Grenade Launcher
// ------------------------------------------------------------
//Firing and ready states have to be redone in the future to allow simultaneous fire
const HDLD_MA75B="m75";
const WEPHELP_MA75MAGMANAGER=WEPHELP_USER3.."  Magazine Manager \(Hold "..WEPHELP_USE.." to manage grenade packs\)""\n";

class WAN_MA75B:HDWeapon{
	default{
		//$Category "Weapons/Hideous Destructor"
		//$Title "Liberator"
		//$Sprite "IBFLB0"

		weapon.slotnumber 6;
		weapon.slotpriority 1.8;
		weapon.kickback 20;
		weapon.selectionorder 27;
		inventory.pickupsound "weapons/WsteM5_pickup";
		//inventory.pickupmessage "You got the MA-75B battle rifle!";
		weapon.bobrangex 0.22;
		weapon.bobrangey 0.9;
		scale 0.6;
		obituary "$WAN_OBIT_MA75B"; //bigger guns nearby
		hdweapon.refid HDLD_MA75B;
		tag "$WAN_TAG_MA75B";
		inventory.icon "M75BA0";
		
		hdweapon.loadoutcodes "
			\cusemi - 0/1, whether it is limited to semi
			\culefty - 0/1, whether brass comes out on left
			\cufiremode - 0/1, whether you start in full auto
			\cunopack - 0/1, whether you start with a grenade pack loaded
			\cuscope - 0/1, enable/disable scope
			\cureflex - 0/1, enable/disable dot sight";
	}
	override string pickupmessage(){
		string msg = Stringtable.Localize("$WAN_PICK_MA75B");
		if(weaponstatus[0]&MA75BF_SCOPE)msg=msg..Stringtable.Localize("$WAN_PICK_MA75BSCOPE");
		return msg;
	}
	ui void ShowZMScope(
		int zoomamt,
		actor hpc,
		HDStatusBar sb,
		int scaledyoffset,
		vector2 bob
	){
		double degree=(zoomamt)*0.1;

		int scaledwidth=57;
		int cx,cy,cw,ch;
		[cx,cy,cw,ch]=screen.GetClipRect();
		sb.SetClipRect(
			-28+bob.x,12+bob.y,scaledwidth,scaledwidth,
			sb.DI_SCREEN_CENTER
		);

			//the new circular view code that doesn't work with LZDoom 3.87c

		sb.fill(color(255,0,0,0),
			bob.x-27,scaledyoffset+bob.y-27,
			54,54,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER
		);

		texman.setcameratotexture(hpc,"HDXCAM_ZM66",degree);
		let cam  = texman.CheckForTexture("HDXCAM_ZM66",TexMan.Type_Any);
		sb.DrawCircle(cam,(0,scaledyoffset)+bob*1.2,.075,usePixelRatio:true);

		sb.SetClipRect(cx,cy,cw,ch);

		sb.drawimage(
			"zm66scop",(0,scaledyoffset)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER,
			scale:(0.82,0.82)
		);
	}
	
	override bool AddSpareWeapon(actor newowner){return AddSpareWeaponRegular(newowner);}
	override hdweapon GetSpareWeapon(actor newowner,bool reverse,bool doselect){return GetSpareWeaponRegular(newowner,reverse,doselect);}
	override void postbeginplay(){
		super.postbeginplay();
		barrelwidth=1;
		barreldepth=4;
		barrellength=32;
		
		weaponspecial=1337;
	}
	override double gunmass(){
		double howmuch=12;
		return howmuch+weaponstatus[MA75BS_MAG]*0.05+weaponstatus[MA75BS_GRENADES]*0.7;
	}
	override double weaponbulk(){
		double blx=151;
		int grenades = weaponstatus[MA75BS_GRENADES];
		if(grenades>=0)blx+=10+ENC_ROCKETLOADED*grenades;
		
		int mgg=weaponstatus[MA75BS_MAG];
		return blx+(mgg<0?0:(ENC_776MAG_LOADED*1.2+mgg*ENC_776_LOADED));
	}
	override string,double getpickupsprite(){
		string spr;

		if(weaponstatus[MA75BS_MAG]<0)spr="B";
		else spr="A";

		return "M75B"..spr.."0",1.;
	}
	override void DrawHUDStuff(HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl){
		if(sb.hudlevel==1){
			int nextmagloaded=sb.GetNextLoadMag(hdmagammo(hpl.findinventory("WAN_MA75BClip"))) % 100;
			int nextpackloaded=sb.GetNextLoadMag(hdmagammo(hpl.findinventory("WAN_GrenadePack")));
			string magsprite;
			if(nextmagloaded>=52){
				sb.drawimage("M75MNORM",(-46,-3),sb.DI_SCREEN_CENTER_BOTTOM);
			}else if(nextmagloaded<1){
				sb.drawimage("M75MEMPTY",(-46,-3),sb.DI_SCREEN_CENTER_BOTTOM,alpha:nextmagloaded?0.6:1.);
			}else sb.drawbar(
				"M75MNORM","M75MGREY",
				nextmagloaded,52,
				(-46,-3),-1,
				sb.SHADER_VERT,sb.DI_SCREEN_CENTER_BOTTOM
			);
			sb.drawnum(hpl.countinv("WAN_MA75BClip"),-43,-8,sb.DI_SCREEN_CENTER_BOTTOM);
			switch (nextpackloaded)
			{
				case 0:
					magsprite="M75GA0";
					break;
				case 1:
					magsprite="M75GB0";
					break;
				case 2:
					magsprite="M75GC0";
					break;
				case 3:
					magsprite="M75GD0";
					break;
				case 4:
					magsprite="M75GE0";
					break;
				case 5:
					magsprite="M75GF0";
					break;
				case 6:
					magsprite="M75GG0";
					break;
				case 7:
					magsprite="M75GH0";
					break;
				default:
					magsprite="M75GA0";
					break;
			}

			if(nextpackloaded>=0){
				sb.drawimage(magsprite,(-62,-4),sb.DI_SCREEN_CENTER_BOTTOM,scale:(0.6,0.6));
			}else sb.drawimage("M75GI0",(-62,-4),sb.DI_SCREEN_CENTER_BOTTOM,alpha:0.6,scale:(0.6,0.6));
			sb.drawnum(hpl.countinv("WAN_GrenadePack"),-56,-8,sb.DI_SCREEN_CENTER_BOTTOM);
		}
		if(!(hdw.weaponstatus[0]&MA75BF_NOAUTO)){
			string llba="RBRSA3A7";
			if(hdw.weaponstatus[0]&MA75BF_FULLAUTO)llba="STFULAUT";
			sb.drawimage(
				llba,(-22,-10),
				sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_TRANSLATABLE|sb.DI_ITEM_RIGHT
			);
		}
		for(int i=hdw.weaponstatus[MA75BS_GRENADES];i>0;i--){
			sb.drawrect(-15-i*3.5,-4,2,2.5);
			sb.drawrect(-14.5-i*3.5,-4.5,1.0,1.0);
		}
		int lod=max(hdw.weaponstatus[MA75BS_MAG],0);
		sb.drawwepnum(lod,52);
		if(hdw.weaponstatus[MA75BS_CHAMBER]==2){
			sb.drawrect(-19,-11,3,1);
			lod++;
		}
	}
	override string gethelptext(){
		return
		WEPHELP_FIRESHOOT
		..WEPHELP_ALTFIRE.."  Shoot GL\n"
		..WEPHELP_RELOAD.."  Reload mag\n"
		..WEPHELP_USE.."+"..WEPHELP_RELOAD.."  Reload chamber\n"
		..WEPHELP_ALTRELOAD.."  Reload GL\n"
		..WEPHELP_MA75MAGMANAGER
		..WEPHELP_UNLOAD.."  Unload magazine \(Hold "..WEPHELP_FIREMODE.." to unload GL\)\n"
		;
	}
	override void DrawSightPicture(
		HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl,
		bool sightbob,vector2 bob,double fov,bool scopeview,actor hpc,string whichdot
	){
		
		vector2 bobb=bob*1.1;
		//bobb.y=clamp(bobb.y,-8,8);
		if(weaponstatus[0]&MA75BF_DTSIT){
			bobb=bob*1.18;
			double dotoff=max(abs(bob.x),abs(bob.y));
			if(dotoff<30){
				string whichdot=sb.ChooseReflexReticle(hdw.weaponstatus[MA75BS_DOT]);
				sb.drawimage(
					whichdot,(0,0)+bobb,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER,
					alpha:0.8-dotoff*0.01,scale:(1.1,1.1),
					col:0xFF000000|sb.crosshaircolor.GetInt()
				);
			}
			sb.drawimage(
				"smgrearsight",(0,0)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER
			);
		}else{
			bobb=bob*1.1;
			int cx,cy,cw,ch;
			[cx,cy,cw,ch]=Screen.GetClipRect();
			sb.SetClipRect(
				-16+bob.x,-32+bob.y,32,38,
				sb.DI_SCREEN_CENTER
			);
			sb.drawimage(
				"ma75fsit",(0,0)+bobb,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
				alpha:0.75,scale:(1.0,1.0)
			);
			sb.SetClipRect(cx,cy,cw,ch);
			sb.drawimage(
				"ma75bsit",(0,3)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
				scale:(1.2,1.2)
			);
		}
		int scaledyoffset=40;
		if(scopeview&&(weaponstatus[0]&MA75BF_SCOPE))ShowZMScope(40,hpc,sb,scaledyoffset,bob);
	}
	override void SetReflexReticle(int which){weaponstatus[MA75BS_DOT]=which;}
	override void failedpickupunload(){
		failedpickupunloadmag(MA75BS_MAG,"WAN_MA75BClip");
	}
	override void DropOneAmmo(int amt){
		if(owner){
			amt=clamp(amt,1,10);
			if(owner.countinv("SevenMilAmmo"))owner.A_DropInventory("SevenMilAmmo",30);
			else{
				double angchange=-10;
				if(angchange)owner.angle-=angchange;
				owner.A_DropInventory("WAN_MA75BClip",1);
				if(angchange){
					owner.angle+=angchange*2;
					owner.A_DropInventory("WAN_GrenadePack",1);
					owner.angle-=angchange;
				}
			}
		}
	}
	override void ForceBasicAmmo(){
		owner.A_TakeInventory("SevenMilAmmo");
		owner.A_TakeInventory("SevenMilBrass");
		owner.A_TakeInventory("FourMilAmmo");
		ForceOneBasicAmmo("WAN_MA75BClip");
		//owner.A_TakeInventory("DudRocketAmmo");
		//owner.A_SetInventory("HDRocketAmmo",1);
	}
	override void tick(){
		super.tick();
		drainheat(MA75BS_HEAT,8);
	}
	action void A_Chamber(bool unloadonly=false){
		A_StartSound("weapons/MA75B_chamber",8,CHANF_OVERLAP);
		actor brsss=null;
		if(invoker.weaponstatus[MA75BS_CHAMBER]==1){
			bool lefty=invoker.weaponstatus[0]&MA75BF_LEFTY;
			int lll=(lefty?1:-1);
			brsss=A_EjectCasing(
				"HDSpent7mm",lll,
				(frandom(0.3,0.6),frandom(2.5*lll,3.5*lll),frandom(0,0.2)),
				(0,0,-3)
			);
			brsss.bseesdaggers=lefty;
			brsss.vel+=vel;
			brsss.A_StartSound(brsss.bouncesound,volume:0.4);
		}else if(invoker.weaponstatus[MA75BS_CHAMBER]==2){
			double fc=max(pitch*0.01,5);
			double cosp=cos(pitch);
			[cosp,brsss]=A_SpawnItemEx(
				invoker.weaponstatus[0]&MA75BF_RECAST?"HDLoose7mmRecast":"HDLoose7mm",
				cosp*12,0,height-8-sin(pitch)*12,
				cosp*fc,0.2*randompick(-1,1),-sin(pitch)*fc,
				0,SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
			);
			brsss.vel+=vel;
			brsss.A_StartSound(brsss.bouncesound,volume:0.4);
		}
		if(!unloadonly && invoker.weaponstatus[MA75BS_MAG]>0){
			invoker.weaponstatus[MA75BS_CHAMBER]=2;
			if(WAN_MA75BClip.CheckRecast(invoker.weaponstatus[MA75BS_MAG],invoker.weaponstatus[MA75BS_RECASTS])){
				invoker.weaponstatus[0]|=MA75BF_RECAST;
				invoker.weaponstatus[MA75BS_RECASTS]--;
			}
			invoker.weaponstatus[MA75BS_MAG]--;
		}else{
			invoker.weaponstatus[0]&=~MA75BF_RECAST;
			invoker.weaponstatus[MA75BS_CHAMBER]=0;
			if(brsss!=null)brsss.vel=vel+(cos(angle),sin(angle),-2);
		}
		A_WeaponReady(WRF_NOFIRE);
	}
	states{
	select0:
		MA75 A 0 A_CheckDefaultReflexReticle(MA75BS_DOT);
		MA75 A 0 A_Overlay(2,"droop");
		goto select0big;
	deselect0:
		MA75 A 0{
			while(invoker.weaponstatus[MA75BS_BRASS]>0){
				double cosp=cos(pitch);
				actor brsss;
				[cosp,brsss]=A_SpawnItemEx("HDSpent7mm",
					cosp*12,0,height-8-sin(pitch)*12,
					cosp*3,0.2*randompick(-1,1),-sin(pitch)*3,
					0,SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
				);
				brsss.vel+=vel;
				brsss.A_StartSound(brsss.bouncesound,volume:0.4);
				invoker.weaponstatus[MA75BS_BRASS]--;
			}
		}goto deselect0big;
	droop:
		TNT1 A 1{
			int grenades = invoker.weaponstatus[MA75BS_GRENADES];
			if(grenades>2&&pitch<(frandom(-2,-4)+grenades*1.4)&&(!gunbraced())){
				A_MuzzleClimb(frandom(-0.06,0.06),
					frandom(0.1,clamp(1-pitch,
						hdplayerpawn(self)?0.06/hdplayerpawn(self).strength:0.06
					,0.12))
				);
			}
		}loop;
	
	ready:
		MA75 A 1 A_WeaponReady(WRF_ALL);
		goto readyend;
	user3:
		---- A 0 {
				if(pressinguse()&&countinv("WAN_GrenadePack"))
					A_MagManager("WAN_GrenadePack");
				else
					A_MagManager("WAN_MA75BClip");
		}
		goto ready;

	fire:
		MA75 A 0 setweaponstate("firegun");

	hold:
		MA75 A 1{
			if(
				!(invoker.weaponstatus[0]&MA75BF_FULLAUTO)
				||(invoker.weaponstatus[0]&MA75BF_NOAUTO)
				||invoker.weaponstatus[MA75BS_CHAMBER]!=2
			)setweaponstate("nope");
		}goto shoot;

	firegun:
		MA75 A 1 A_SetTics(0);
	shoot:
		MA75 A 1{
			if(invoker.weaponstatus[MA75BS_CHAMBER]==2)A_Gunflash();
			else setweaponstate("chamber_manual");
			A_WeaponReady(WRF_NOFIRE);
		}
		MA75  A 1 A_Chamber();
		MA75  A 0 A_Refire();
		goto nope;
	flash:
		MA75 B 1 bright{
			A_Light1();
			A_StartSound("weapons/MA75B_fire",CHAN_WEAPON);
			double gunheat = invoker.weaponstatus[MA75BS_HEAT];
			gunheat = clamp(gunheat,0,34)*0.6;

			HDBulletActor.FireBullet(self,
				invoker.weaponstatus[0]&MA75BF_RECAST?"HDB_776r":"HDB_776",
				spread:gunheat//invoker.weaponstatus[0]&MA75BF_FULLAUTO?10:1
			);
			HDFlashAlpha(16);
			A_ZoomRecoil(0.90);
			if(invoker.weaponstatus[MA75BS_GRENADES]>2){
				A_MuzzleClimb(
					0,0,
					-0.07,-0.14,
					-frandom(0.3,0.6),-frandom(0.9,1.3),
					-frandom(0.2,0.4),-frandom(0.9,1.3)
				);
			}else{
				A_MuzzleClimb(
					0,0,
					-0.07,-0.14,
					-frandom(0.3,0.6),-frandom(1.1,1.5),
					-frandom(0.2,0.4),-frandom(1.1,1.5)
				);
			}

			invoker.weaponstatus[MA75BS_CHAMBER]=1;
			invoker.weaponstatus[MA75BS_HEAT]+=random(3,5);
			invoker.weaponstatus[0]&=~MA75BF_RECAST;
			A_AlertMonsters();
		}
		goto lightdone;
	chamber_manual:
		MA75 A 1 offset(-1,34){
			if(
				invoker.weaponstatus[MA75BS_CHAMBER]==2
				||invoker.weaponstatus[MA75BS_MAG]<1
			)setweaponstate("nope");
		}
		MA75 A 1 offset(-2,36)A_Chamber();
		MA75 A 1 offset(-2,38);
		MA75 A 1 offset(-1,34);
		goto nope;

	firemode:
		MA75 A 1{
			invoker.weaponstatus[0]^=MA75BF_FULLAUTO;
			A_WeaponReady(WRF_NONE);
		}
	firemodehold:
		MA75 A 1{
			if(pressingunload()
			&&(invoker.weaponstatus[MA75BS_GRENADES]>=0)){
				invoker.weaponstatus[0]^=MA75BF_FULLAUTO;
				invoker.weaponstatus[MA75BS_FLAGS]|=MA75BF_UNLOADONLY;
				setweaponstate("unpack");
			}else A_WeaponReady(WRF_NONE);
		}
		MA75 A 0 A_JumpIf(pressingfiremode()&&(invoker.weaponstatus[MA75BS_GRENADES]>=0),"firemodehold");
		goto nope;

	unloadchamber:
		MA75 A 1 offset(-1,34){
			if(
				invoker.weaponstatus[MA75BS_CHAMBER]<1
			)setweaponstate("nope");
		}
		MA75 A 1 offset(-2,36)A_Chamber(true);
		MA75 A 1 offset(-2,38);
		MA75 A 1 offset(-1,34);
		goto nope;

	loadchamber:
		MA75 A 0 A_JumpIf(invoker.weaponstatus[MA75BS_CHAMBER]>0,"nope");
		MA75 A 0 A_JumpIf(
			!countinv("SevenMilAmmo")
			&&!countinv("SevenMilAmmoRecast")
		,"nope");
		MA75 A 1 offset(0,34) A_StartSound("weapons/pocket",9);
		MA75 A 2 offset(2,36);
		MA75 A 8 offset(5,40);
		MA75 A 8 offset(7,44);
		MA75 A 8 offset(6,43);
		MA75 A 10 offset(4,39){
			class<inventory> rndtp="SevenMilAmmo";
			if(!countinv(rndtp))rndtp="SevenMilAmmoRecast";
			if(countinv(rndtp)){
				A_TakeInventory(rndtp,1,TIF_NOTAKEINFINITE);
				invoker.weaponstatus[MA75BS_CHAMBER]=2;

				if(rndtp=="SevenMilAmmoRecast")invoker.weaponstatus[0]|=MA75BF_RECAST;
				else invoker.weaponstatus[0]&=~MA75BF_RECAST;
				A_StartSound("weapons/MA75B_chamber2",8);
				A_StartSound("weapons/MA75B_chamber2a",8,CHANF_OVERLAP,0.7);
			}else A_SetTics(4);
		}
		MA75 A 7 offset(5,37);
		MA75 A 1 offset(2,36);
		MA75 A 1 offset(0,34);
		goto readyend;

	user4:
	unload:
		---- A 1; //DO NOT set this frame to zero
		MA75 C 0{
			invoker.weaponstatus[0]|=MA75BF_UNLOADONLY;
			if(
				invoker.weaponstatus[MA75BS_MAG]>=0  
			){
				return resolvestate("unmag");
			}else if(
				invoker.weaponstatus[MA75BS_CHAMBER]>0  
			){
				return resolvestate("unloadchamber");
			}
			return resolvestate("nope");
		}
	reload:
		MA75 C 0{
			int inmag=invoker.weaponstatus[MA75BS_MAG];
			bool nomags=HDMagAmmo.NothingLoaded(self,"WAN_MA75BClip");
			bool haverounds=countinv("SevenMilAmmo")||countinv("SevenMilAmmoRecast");
			invoker.weaponstatus[0]&=~MA75BF_UNLOADONLY;

			//no point reloading
			if(
						
				inmag>=52
				||(
					//no mags to load and can't directly load chamber
					nomags
					&&(
						!haverounds
						||inmag>=0
						||invoker.weaponstatus[MA75BS_CHAMBER]>0
								 
					)
				)
			)return resolvestate("nope");

			//no mag, empty chamber, have loose rounds
			if(
				inmag<0
				&&invoker.weaponstatus[MA75BS_CHAMBER]<1
				&&haverounds
				&&(
					pressinguse()
					||nomags
				)
			)return resolvestate("loadchamber");
			else if(
				invoker.weaponstatus[MA75BS_MAG]>0  
			){
				//if full mag and unchambered, chamber
				if(
					invoker.weaponstatus[MA75BS_MAG]>=52 
					&&invoker.weaponstatus[MA75BS_CHAMBER]!=2
				){
					return resolvestate("chamber_manual");
				}				
			}
			return resolvestate("unmag");
		}

	unmag:
		MA75 C 1 offset(0,34);
		MA75 C 1 offset(2,36);
		MA75 D 1 offset(4,40);
		MA75 E 2 offset(8,42){
			A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
			A_StartSound("weapons/rifleclick2",8);
		}
		MA75 F 4 offset(14,46){
			A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
			A_StartSound ("weapons/rifleload",8,CHANF_OVERLAP);
		}
		MA75 F 0{
			int magamt=invoker.weaponstatus[MA75BS_MAG];
			if(magamt<0){setweaponstate("magout");return;}

			if(magamt>0){
				int fullets=clamp(52-invoker.weaponstatus[MA75BS_RECASTS],0,magamt);
				magamt+=fullets*100;
			}

			invoker.weaponstatus[MA75BS_MAG]=-1;
			invoker.weaponstatus[MA75BS_RECASTS]=0;
			if(
				!PressingReload()
				&&!PressingUnload()
			){
				HDMagAmmo.SpawnMag(self,"WAN_MA75BClip",magamt);
				setweaponstate("magout");
			}else{
				HDMagAmmo.GiveMag(self,"WAN_MA75BClip",magamt);
				setweaponstate("pocketmag");
			}
		}
	pocketmag:
		MA75 F 7 offset(12,52)A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		MA75 F 0 A_StartSound("weapons/pocket",9);
		MA75 FF 7 offset(14,54)A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		MA75 F 0{
		}goto magout;
	magout:
		MA75 F 4{
			invoker.weaponstatus[MA75BS_MAG]=-1;
			if(invoker.weaponstatus[0]&MA75BF_UNLOADONLY)setweaponstate("reloaddone");
		}goto loadmag;


	loadmag:
		MA75 F 0 A_StartSound("weapons/pocket",9);
		MA75 G 7 A_MuzzleClimb(frandom(-0.2,0.4),frandom(-0.2,0.8));
		MA75 G 9 A_StartSound("weapons/MA75B_reload1",8,CHANF_OVERLAP);
		MA75 H 8; //A_StartSound("weapons/MA75B_reload1",8,CHANF_OVERLAP);
		MA75 I 6 {
			let mmm=hdmagammo(findinventory("WAN_MA75BClip"));
			if(mmm){
				int minput=mmm.TakeMag(true);
				int rndcnt=minput%100;
				invoker.weaponstatus[MA75BS_MAG]=rndcnt;
				invoker.weaponstatus[MA75BS_RECASTS]=clamp(52-(minput/100),0,rndcnt);
				//A_StartSound("weapons/rifleclick",8);
				A_StartSound("weapons/MA75B_reload2",8,CHANF_OVERLAP);
			}
		}
		MA75 J 4; //A_StartSound("weapons/MA75B_reload2",8,CHANF_OVERLAP);
		goto reloaddone;

	reloaddone:
		MA75 K 3;
		MA75 L 2;
		goto chamber_manual;


	altfire:
		MA75 A 0 A_JumpIf(invoker.weaponstatus[MA75BS_GRENADES]>0,1);
		goto nope;
		MA75 A 2;
		MA75 A 3 A_Gunflash("firegrenade");
		MA75 A 5;
		goto nope;
	althold:
		MA75 A 0;
		goto nope;


	firegrenade:
		TNT1 A 0 A_JumpIf(invoker.weaponstatus[MA75BS_GRENADES]>0,1);
		stop;
		TNT1 A 2{
			invoker.airburst=0;
			A_FireHDGL();
			invoker.weaponstatus[MA75BS_GRENADES]-=1;
			A_StartSound("weapons/MA75B_grenade",CHAN_WEAPON);
			A_ZoomRecoil(0.95);
		}
		TNT1 A 2 A_MuzzleClimb(
			0,0,0,0,
			-0.8,-2.,
			-0.4,-1.
		);
		stop;

	altreload:
		MA75 A 0{
			int inpak=invoker.weaponstatus[MA75BS_GRENADES];
			bool nopaks=HDMagAmmo.NothingLoaded(self,"WAN_GrenadePack");
			invoker.weaponstatus[0]&=~MA75BF_UNLOADONLY;

			//no point reloading
			if(	
				inpak>=7
				||(
					//no mags to load and can't directly load chamber
					nopaks
				)
			)return resolvestate("nope");
			return resolvestate("unpack");
		}
	unpack:
		MA75 A 1 offset(0,34);
		MA75 A 1 offset(2,36);
		MA75 A 1 offset(4,40);
		MA75 A 2 offset(8,42){
			A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
			A_StartSound("weapons/rifleclick2",8);
		}
		MA75 A 4 offset(14,46){
			A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
			A_StartSound ("weapons/grenopen",8,CHANF_OVERLAP);
		}
		MA75 A 0{
			int grenamt=invoker.weaponstatus[MA75BS_GRENADES];
			if(grenamt<0){setweaponstate("packout");return;}

			invoker.weaponstatus[MA75BS_GRENADES]=-1;
			if(
				!PressingAltReload()
				&&!PressingUnload()
			){
				HDMagAmmo.SpawnMag(self,"WAN_GrenadePack",grenamt);
				setweaponstate("packout");
			}else{
				HDMagAmmo.GiveMag(self,"WAN_GrenadePack",grenamt);
				setweaponstate("pocketpack");
			}
		}
	pocketpack:
		MA75 A 7 offset(12,52)A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		MA75 A 0 A_StartSound("weapons/pocket",9);
		MA75 AA 7 offset(14,54)A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		MA75 A 0{
		}goto packout;
	packout:
		MA75 A 4{
			invoker.weaponstatus[MA75BS_GRENADES]=-1;
			if(invoker.weaponstatus[0]&MA75BF_UNLOADONLY)setweaponstate("altreloaddone");
		}goto loadpack;	
	loadpack:
		MA75 A 0 A_StartSound("weapons/pocket",9);
		MA75 AA 6 offset(14,54)A_MuzzleClimb(frandom(-0.2,0.4),frandom(-0.2,0.8));
		MA75 A 2 offset(13,52){
			let mmm=hdmagammo(findinventory("WAN_GrenadePack"));
			if(mmm){
				int minput=mmm.TakeMag(true);
				int rndcnt=minput;
				invoker.weaponstatus[MA75BS_GRENADES]=rndcnt;
				//A_StartSound("weapons/rifleclick",8);
				A_StartSound("weapons/grenreload",8,CHANF_OVERLAP);
			}
		}
		MA75 A 2 offset(13,54);
		MA75 A 3 offset(14,57);
		MA75 A 2 offset(14,55);
		MA75 A 15 offset(14,54);
		MA75 A 2 offset(12,52);
		MA75 A 2 offset(10,50);
		MA75 A 2 offset(8,46) A_StartSound("weapons/MA75B_click",8,CHANF_OVERLAP);
		MA75 A 1 offset(10,44);
		MA75 A 2 offset(13,42);
		MA75 A 2 offset(11,43);
		MA75 A 3 offset(8,45);
		goto altreloaddone;

	altreloaddone:
		MA75 A 1 offset (4,40);
		MA75 A 1 offset (2,34);
		goto ready;

	spawn:
		M75B AB -1 nodelay{

			if(invoker.weaponstatus[MA75BS_MAG]<0)frame=1;
			else frame=0;

		}
		M75B AB -1;
		stop;
	}
	override void InitializeWepStats(bool idfa){
		weaponstatus[MA75BS_MAG]=52;
		weaponstatus[MA75BS_GRENADES]=7;
		weaponstatus[MA75BS_RECASTS]=0;
		weaponstatus[MA75BS_CHAMBER]=2;
		if(!idfa&&!owner){
			weaponstatus[MA75BS_HEAT]=0;
		}
	}
	override void loadoutconfigure(string input){

		int firemode=getloadoutvar(input,"firemode",1);
		if(firemode>0)weaponstatus[0]&=~MA75BF_FULLAUTO; //|=MA75BF_FULLAUTO;
		else weaponstatus[0]|=MA75BF_FULLAUTO; //&=~MA75BF_FULLAUTO;

		int semi=getloadoutvar(input,"semi",1);
		if(semi>0){
			weaponstatus[0]|=MA75BF_NOAUTO;
			weaponstatus[0]&=~MA75BF_FULLAUTO;
		}else weaponstatus[0]&=~MA75BF_NOAUTO;
		
		int lefty=getloadoutvar(input,"lefty",1);
		if(lefty>0)weaponstatus[0]|=MA75BF_LEFTY;
		else weaponstatus[0]&=~MA75BF_LEFTY;
		
		int nopack=getloadoutvar(input,"nopack",1);
		if(nopack>0)weaponstatus[MA75BS_GRENADES]=-1;
		else weaponstatus[MA75BS_GRENADES]=7;
		
		int scope=getloadoutvar(input,"scope",1);
		if(scope>0)weaponstatus[0]|=MA75BF_SCOPE;
		else weaponstatus[0]&=~MA75BF_SCOPE;
		
		int xhdot=getloadoutvar(input,"dot",3);
		int reflex=getloadoutvar(input,"reflex",1);
		if(
			!reflex
			&&xhdot<0
		)weaponstatus[0]&=~MA75BF_DTSIT;
		else{
			weaponstatus[0]|=MA75BF_DTSIT;
			if(xhdot>=0)weaponstatus[MA75BS_DOT]=xhdot;
		}
	}
}
enum ma75bstatus{
	MA75BF_FULLAUTO=1,
	MA75BF_SCOPE=2,
	MA75BF_UNLOADONLY=4,
	MA75BF_NOAUTO=8,
	MA75BF_LEFTY=16,
	MA75BF_RECAST=32,
	MA75BF_DTSIT=64,

	MA75BS_FLAGS=0,
	MA75BS_CHAMBER=1,
	MA75BS_MAG=2, //-1 is empty
	MA75BS_HEAT=3,
	MA75BS_BRASS=4,
	MA75BS_RECASTS=5,
	MA75BS_DOT=6,
	MA75BS_GRENADES=7,
};

class WAN_MA75BClip:HD7mMag{
	default{
		hdmagammo.maxperunit 5252;
		hdmagammo.roundtype "SevenMilAmmo";
		hdmagammo.roundbulk ENC_776_LOADED;
		hdmagammo.magbulk ENC_776MAG_EMPTY*1.2;
		hdpickup.refid '752';
		tag "$WAN_TAG_MA75BCLIP";
		//inventory.pickupmessage "Picked up a MA-75B clip.";
		scale 0.8;
	}
	
	override string pickupmessage() {return Stringtable.Localize("$WAN_PICK_MA75BCLIP");}
	
	override string,string,name,double getmagsprite(int thismagamt){
		string magsprite=(thismagamt>0)?"M75MA0":"M75MB0";
		return magsprite,"RBRSA3A7","SevenMilAmmo",1.5;
	}
	
	override void GetItemsThatUseThis(){
		itemsthatusethis.push("WAN_MA75B");
	}
	
	states(actor){
	spawn:
		M75M A -1;
		stop;
	spawnempty:
		M75M B -1{
			brollsprite=true;brollcenter=true;
			roll=randompick(0,0,0,0,2,2,2,2,1,3)*90;
		}stop;
	}
}

class WAN_GrenadePack:HDMagAmmo{
	default{
		hdmagammo.maxperunit 7;
		hdmagammo.roundtype "HDRocketAmmo";
		hdmagammo.roundbulk ENC_ROCKETLOADED;
		hdmagammo.magbulk 15;
		hdmagammo.inserttime 12;
		hdmagammo.extracttime 10;
		tag "$WAN_TAG_MA75BPACK";
		//inventory.pickupmessage "Picked up a MA-75B grenade pack.";
		hdpickup.refid 'rk7';
		scale 0.4;
	}
	
	override string pickupmessage() {return Stringtable.Localize("$WAN_PICK_MA75BPACK");}
	
	override string,string,name,double getmagsprite(int thismagamt){
		string magsprite;
		switch (thismagamt)
		{
			case 0:
				magsprite="M75GA0";
				break;
			case 1:
				magsprite="M75GB0";
				break;
			case 2:
				magsprite="M75GC0";
				break;
			case 3:
				magsprite="M75GD0";
				break;
			case 4:
				magsprite="M75GE0";
				break;
			case 5:
				magsprite="M75GF0";
				break;
			case 6:
				magsprite="M75GG0";
				break;
			case 7:
				magsprite="M75GH0";
				break;
			default:
				magsprite="M75GA0";
				break;
		}
		return magsprite,"ROQPA0","HDRocketAmmo",0.6;
	}
	
	override void GetItemsThatUseThis(){
		itemsthatusethis.push("WAN_MA75B");
	}
	
	states(actor){
	spawn:
		M75G ABCDEFGH -1 nodelay{
			if(!mags.size()){destroy();return;}
			int amt=mags[0];
			frame=amt;
		}stop;
	spawnempty:
		M75G A -1{
			//brollsprite=true;brollcenter=true;
			//roll=randompick(0,0,0,0,2,2,2,2,1,3)*90;
		}stop;
	}
}

class WAN_MA75BSpawner:IdleDummy{
	states{
	spawn:
		TNT1 A 0 nodelay{
			let www=WAN_MA75B(spawn("WAN_MA75B",pos,ALLOW_REPLACE));
			if(!www)return;
			HDF.TransferSpecials(self, www);
			if(!random(0,5))www.weaponstatus[0]|=MA75BF_SCOPE;
			if(!random(0,2))www.weaponstatus[0]|=MA75BF_DTSIT;

			spawn("WAN_GrenadePack",pos+(10,0,0),ALLOW_REPLACE);
			spawn("WAN_MA75BClip",pos+(7,0,0),ALLOW_REPLACE);
			spawn("WAN_MA75BClip",pos+(5,0,0),ALLOW_REPLACE);
		}stop;
	}
}

class WAN_GrenadePackSpawner:IdleDummy{
	states{
	spawn:
		TNT1 A 0 nodelay{
			A_SpawnItemEx("WAN_GrenadePack",1,0,0,0,0,0,0,0,0);
			//A_SpawnItemEx("WAN_CustomRocketSpawner",10,0,0,0,0,0,0,0,180);
		}stop;
	}
}
