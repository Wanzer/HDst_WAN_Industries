// ------------------------------------------------------------
// MA-76C Assault Rifle with integral 20mm Grenade Launcher
// ------------------------------------------------------------
//Firing and ready states have to be redone in the future to allow simulatneous fire
const HDLD_MA76C="m76";

class WAN_MA76C:WAN_20mmLauncher{
	default{
		//$Category "Weapons/Hideous Destructor"

		+hdweapon.fitsinbackpack
		weapon.slotnumber 4;
		weapon.slotpriority 1.8;
		weapon.kickback 20;
		weapon.selectionorder 27;
		inventory.pickupsound "weapons/WsteM5_pickup";
		//inventory.pickupmessage "You got the MA-76C assault rifle!";
		weapon.bobrangex 0.22;
		weapon.bobrangey 0.9;
		scale 0.6;
		obituary "$WAN_OBIT_MA76C"; //bigger guns nearby
		hdweapon.refid HDLD_MA76C;
		tag "$WAN_TAG_MA76C";
		inventory.icon "M76CA0";
		
		hdweapon.loadoutcodes "
			\cusemi - 0/1, whether it is limited to semi
			\cufiremode - 0/1, whether you start in full auto
			\cunopack - 0/1, whether you start with a grenade pack loaded
			\cuscope - 0/1, enable/disable scope
			\cureflex - 0/1, enable/disable dot sight";
	}
	override string pickupmessage(){
		string msg = Stringtable.Localize("$WAN_PICK_MA76C");
		if(weaponstatus[0]&MA76CF_SCOPE)msg=msg..Stringtable.Localize("$WAN_PICK_MA76CSCOPE");
		return msg;
	}
	ui void ShowZMScope(
		int zoomamt,
		actor hpc,
		HDStatusBar sb,
		int scaledyoffset,
		vector2 bob
	){
		double degree=(zoomamt)*0.1;

		int scaledwidth=57;
		int cx,cy,cw,ch;
		[cx,cy,cw,ch]=screen.GetClipRect();
		sb.SetClipRect(
			-28+bob.x,12+bob.y,scaledwidth,scaledwidth,
			sb.DI_SCREEN_CENTER
		);

			//the new circular view code that doesn't work with LZDoom 3.87c

		sb.fill(color(255,0,0,0),
			bob.x-27,scaledyoffset+bob.y-27,
			54,54,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER
		);

		texman.setcameratotexture(hpc,"HDXCAM_ZM66",degree);
		let cam  = texman.CheckForTexture("HDXCAM_ZM66",TexMan.Type_Any);
		sb.DrawCircle(cam,(0,scaledyoffset)+bob*1.2,.075,usePixelRatio:true);

		sb.SetClipRect(cx,cy,cw,ch);

		sb.drawimage(
			"zm66scop",(0,scaledyoffset)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER,
			scale:(0.82,0.82)
		);
		//sb.drawstring(
		//	sb.mAmountFont,string.format("%.1f",degree),
		//	(6+bob.x,74+bob.y),sb.DI_SCREEN_CENTER|sb.DI_TEXT_ALIGN_RIGHT,
		//	Font.CR_BLACK
		//);
	}
	
	override bool AddSpareWeapon(actor newowner){return AddSpareWeaponRegular(newowner);}
	override hdweapon GetSpareWeapon(actor newowner,bool reverse,bool doselect){return GetSpareWeaponRegular(newowner,reverse,doselect);}
	override void postbeginplay(){
		super.postbeginplay();
		barrelwidth=1;
		barreldepth=3;
		barrellength=26;
		bfitsinbackpack=true;
		
		weaponspecial=1337;
	}
	override double gunmass(){
		double howmuch=8;
		return howmuch+weaponstatus[MA76CS_MAG]*0.01+weaponstatus[MA76CS_GRENADES]*0.3;
	}
	override double weaponbulk(){
		double blx=133;
		int grenades = weaponstatus[MA76CS_GRENADES];
		if(grenades>=0)blx+=5+ENC_20MMGRENADELOADED*grenades;
		
		int mgg=weaponstatus[MA76CS_MAG];
		return blx+(mgg<0?0:(ENC_426MAG_LOADED*1.2+mgg*ENC_426_LOADED));
	}
	override string,double getpickupsprite(){
		string spr;

		if(weaponstatus[MA76CS_MAG]<0)spr="B";
		else spr="A";

		return "M76C"..spr.."0",1.;
	}
	override void DrawHUDStuff(HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl){
		if(sb.hudlevel==1){
			int nextmagloaded=sb.GetNextLoadMag(hdmagammo(hpl.findinventory("WAN_MA76CClip")));
			int nextpackloaded=sb.GetNextLoadMag(hdmagammo(hpl.findinventory("WAN_20mmGrenadePack")));
			string magsprite;
			if(nextmagloaded>=52){
				sb.drawimage("M76MNORM",(-46,-3),sb.DI_SCREEN_CENTER_BOTTOM);
			}else if(nextmagloaded<1){
				sb.drawimage("M76MEMPTY",(-46,-3),sb.DI_SCREEN_CENTER_BOTTOM,alpha:nextmagloaded?0.6:1.);
			}else sb.drawbar(
				"M76MNORM","M76MGREY",
				nextmagloaded,52,
				(-46,-3),-1,
				sb.SHADER_VERT,sb.DI_SCREEN_CENTER_BOTTOM
			);
			sb.drawnum(hpl.countinv("WAN_MA76CClip"),-43,-8,sb.DI_SCREEN_CENTER_BOTTOM);
			switch (nextpackloaded)
			{
				case 0:
					magsprite="M76GA0";
					break;
				case 1:
					magsprite="M76GB0";
					break;
				case 2:
					magsprite="M76GC0";
					break;
				case 3:
					magsprite="M76GD0";
					break;
				case 4:
					magsprite="M76GE0";
					break;
				case 5:
					magsprite="M76GF0";
					break;
				case 6:
					magsprite="M76GG0";
					break;
				case 7:
					magsprite="M76GH0";
					break;
				default:
					magsprite="M76GA0";
					break;
			}

			if(nextpackloaded>=0){
				sb.drawimage(magsprite,(-62,-4),sb.DI_SCREEN_CENTER_BOTTOM,scale:(0.6,0.6));
			}else sb.drawimage("M76GI0",(-62,-4),sb.DI_SCREEN_CENTER_BOTTOM,alpha:0.6,scale:(0.6,0.6));
			sb.drawnum(hpl.countinv("WAN_20mmGrenadePack"),-56,-8,sb.DI_SCREEN_CENTER_BOTTOM);
		}
		if(!(hdw.weaponstatus[0]&MA76CF_NOAUTO)){
			string llba="RBRSA3A7";
			if(hdw.weaponstatus[0]&MA76CF_FULLAUTO)llba="STFULAUT";
			sb.drawimage(
				llba,(-22,-10),
				sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_TRANSLATABLE|sb.DI_ITEM_RIGHT
			);
		}
		for(int i=hdw.weaponstatus[MA76CS_GRENADES];i>0;i--){
			sb.drawrect(-15-i*3.5,-4,2,2.5);
			sb.drawrect(-14.5-i*3.5,-4.5,1.0,1.0);
		}
		int lod=max(hdw.weaponstatus[MA76CS_MAG],0);
		sb.drawwepnum(lod,52);
		if(hdw.weaponstatus[MA76CS_CHAMBER]==2){
			sb.drawrect(-19,-11,3,1);
			lod++;
		}
	}
	override string gethelptext(){
		return
		WEPHELP_FIRESHOOT
		..WEPHELP_ALTFIRE.."  Shoot GL\n"
		..WEPHELP_RELOAD.."  Reload mag\n"
		..WEPHELP_ALTRELOAD.."  Reload GL\n"
		..WEPHELP_MA75MAGMANAGER
		..WEPHELP_UNLOAD.."  Unload magazine \(Hold "..WEPHELP_FIREMODE.." to unload GL\)\n"
		;
	}
	override void DrawSightPicture(
		HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl,
		bool sightbob,vector2 bob,double fov,bool scopeview,actor hpc,string whichdot
	){
		
		vector2 bobb=bob*1.1;
		//bobb.y=clamp(bobb.y,-8,8);
		if(weaponstatus[0]&MA76CF_DTSIT){
			bobb=bob*1.18;
			double dotoff=max(abs(bob.x),abs(bob.y));
			if(dotoff<30){
				string whichdot=sb.ChooseReflexReticle(hdw.weaponstatus[MA76CS_DOT]);
				sb.drawimage(
					whichdot,(0,0)+bobb,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER,
					alpha:0.8-dotoff*0.01,scale:(1.1,1.1),
					col:0xFF000000|sb.crosshaircolor.GetInt()
				);
			}
			sb.drawimage(
				"smgrearsight",(0,0)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER
			);
		}else{
			bobb=bob*1.1;
			int cx,cy,cw,ch;
			[cx,cy,cw,ch]=Screen.GetClipRect();
			sb.SetClipRect(
				-16+bob.x,-32+bob.y,32,38,
				sb.DI_SCREEN_CENTER
			);
			sb.drawimage(
				"ma75fsit",(0,0)+bobb,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
				alpha:0.75,scale:(1.0,1.0)
			);
			sb.SetClipRect(cx,cy,cw,ch);
			sb.drawimage(
				"ma75bsit",(0,3)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
				scale:(1.2,1.2)
			);
		}
		int scaledyoffset=40;
		if(scopeview&&(weaponstatus[0]&MA76CF_SCOPE))ShowZMScope(40,hpc,sb,scaledyoffset,bob);
	}
	override void SetReflexReticle(int which){weaponstatus[MA76CS_DOT]=which;}
	override void failedpickupunload(){
		failedpickupunloadmag(MA76CS_MAG,"WAN_MA76CClip");
	}
	override void DropOneAmmo(int amt){
		if(owner){
			amt=clamp(amt,1,10);
			if(owner.countinv("FourMilAmmo"))owner.A_DropInventory("FourMilAmmo",30);
			else{
				double angchange=-10;
				if(angchange)owner.angle-=angchange;
				owner.A_DropInventory("WAN_MA76CClip",1);
				if(angchange){
					owner.angle+=angchange*2;
					owner.A_DropInventory("WAN_20mmGrenadePack",1);
					owner.angle-=angchange;
				}
			}
		}
	}
	override void ForceBasicAmmo(){
		owner.A_TakeInventory("FourMilAmmo");
		ForceOneBasicAmmo("WAN_MA76CClip");
		//owner.A_TakeInventory("DudRocketAmmo");
		//owner.A_SetInventory("HDRocketAmmo",1);
	}
	override void tick(){
		super.tick();
		drainheat(MA76CS_HEAT,12,1,3,0.3);
	}
	action void A_Chamber(bool unloadonly=false){
		//A_StartSound("weapons/MA76C_chamber",8,CHANF_OVERLAP);
		if(invoker.weaponstatus[MA76CS_CHAMBER]==2){
			double fc=max(pitch*0.01,5);
			double cosp=cos(pitch);
			A_SpawnItemEx(
				"ZM66DroppedRound",
				cosp*12,0,height-8-sin(pitch)*12,
				cosp*fc,0.2*randompick(-1,1),-sin(pitch)*fc,
				0,SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
			);
		}
		if(!unloadonly && invoker.weaponstatus[MA76CS_MAG]>0){
			invoker.weaponstatus[MA76CS_CHAMBER]=2;
			invoker.weaponstatus[MA76CS_MAG]--;
		}else{
			invoker.weaponstatus[MA76CS_CHAMBER]=0;
		}
		A_WeaponReady(WRF_NOFIRE);
	}
	states{
	select0:
		MA76 A 0 A_CheckDefaultReflexReticle(MA76CS_DOT);
		MA76 A 0;
		goto select0big;
	deselect0:
		MA76 A 0;
		goto deselect0big;
	
	ready:
		MA76 A 1 A_WeaponReady(WRF_ALL);
		goto readyend;
	user3:
		---- A 0 {
				if(pressinguse()&&countinv("WAN_20mmGrenadePack"))
					A_MagManager("WAN_20mmGrenadePack");
				else
					A_MagManager("WAN_MA76CClip");
		}
		goto ready;

	fire:
		MA76 A 0 setweaponstate("firegun");

	hold:
		MA76 A 1{
			if(
				!(invoker.weaponstatus[0]&MA76CF_FULLAUTO)
				||(invoker.weaponstatus[0]&MA76CF_NOAUTO)
				||invoker.weaponstatus[MA76CS_CHAMBER]!=2
			)setweaponstate("nope");
		}goto shoot;

	firegun:
		MA76 A 1 A_SetTics(0);
	shoot:
		MA76 A 1{
			A_WeaponOffset (0,34);
			if(invoker.weaponstatus[MA76CS_CHAMBER]==2)A_Gunflash();
			else setweaponstate("chamber_manual");
			A_WeaponReady(WRF_NOFIRE);
		}
		MA76 A 0 A_WeaponOffset (0,32);
		MA76 A 1 A_Chamber();
		MA76 A 0 A_Refire();
		goto nope;
	flash:
		MA76 B 1 bright{
			A_WeaponOffset (0,34);
			A_Light1();
			A_StartSound("weapons/MA76C_fire",CHAN_WEAPON);
			double gunheat = invoker.weaponstatus[MA76CS_HEAT];
			gunheat = clamp(gunheat,0,34)*0.6;

			HDBulletActor.FireBullet(self,
				"HDB_426",
				spread:gunheat//invoker.weaponstatus[0]&MA76CF_FULLAUTO?10:1
			);
			HDFlashAlpha(16);
			A_ZoomRecoil(0.95);
			if(invoker.weaponstatus[MA76CS_GRENADES]>2){
				A_MuzzleClimb(
					-frandom(0.1,0.1),-frandom(0,0.1),
					-0.1,-frandom(0.2,0.3),
					-frandom(0.2,0.8),-frandom(1.0,1.6)
				);
			}else{
				A_MuzzleClimb(
					-frandom(0.1,0.1),-frandom(0,0.1),
					-0.2,-frandom(0.3,0.4),
					-frandom(0.3,1.0),-frandom(1.3,2.0)
				);
			}

			invoker.weaponstatus[MA76CS_CHAMBER]=1;
			invoker.weaponstatus[MA76CS_HEAT]+=random(3,5);
			A_AlertMonsters();
		}
		MA76 B 0 A_WeaponOffset (0,32);
		goto lightdone;
	chamber_manual:
		MA76 A 1 offset(-1,34){
			if(
				invoker.weaponstatus[MA76CS_CHAMBER]==2
				||invoker.weaponstatus[MA76CS_MAG]<1
			)setweaponstate("nope");
		}
		MA76 A 1 offset(-2,36)A_Chamber();
		MA76 A 1 offset(-2,38)A_StartSound("weapons/MA76C_chamber",8,CHANF_OVERLAP);
		MA76 A 1 offset(-1,34);
		goto nope;


	firemode:
		MA76 A 1{
			invoker.weaponstatus[0]^=MA76CF_FULLAUTO;
			A_WeaponReady(WRF_NONE);
		}
	firemodehold:
		MA76 A 1{
			if(pressingunload()
			&&(invoker.weaponstatus[MA76CS_GRENADES]>=0)){
				invoker.weaponstatus[0]^=MA76CF_FULLAUTO;
				invoker.weaponstatus[MA76CS_FLAGS]|=MA76CF_UNLOADONLY;
				setweaponstate("unpack");
			}else A_WeaponReady(WRF_NONE);
		}
		MA76 A 0 A_JumpIf(pressingfiremode()&&(invoker.weaponstatus[MA76CS_GRENADES]>=0),"firemodehold");
		goto nope;

	unloadchamber:
		MA76 A 1 offset(-1,34){
			if(
				invoker.weaponstatus[MA76CS_CHAMBER]<1
			)setweaponstate("nope");
		}
		MA76 A 1 offset(-2,36)A_Chamber(true);
		MA76 A 1 offset(-2,38);
		MA76 A 1 offset(-1,34);
		goto nope;

	loadchamber: //serves no purpose currently, keeping in the event the ma76 moves back to a cased ammo in the future
		MA76 A 0 A_JumpIf(invoker.weaponstatus[MA76CS_CHAMBER]>0,"nope");
		MA76 A 0 A_JumpIf(
			!countinv("FourMilAmmo")
		,"nope");
		MA76 A 1 offset(0,34) A_StartSound("weapons/pocket",9);
		MA76 A 2 offset(2,36);
		MA76 A 8 offset(5,40);
		MA76 A 8 offset(7,44);
		MA76 A 8 offset(6,43);
		MA76 A 10 offset(4,39){
			class<inventory> rndtp="FourMilAmmo";
			if(countinv(rndtp)){
				A_TakeInventory(rndtp,1,TIF_NOTAKEINFINITE);
				invoker.weaponstatus[MA76CS_CHAMBER]=2;
				A_StartSound("weapons/MA75B_chamber2",8);
				A_StartSound("weapons/MA75B_chamber2a",8,CHANF_OVERLAP,0.7);
			}else A_SetTics(4);
		}
		MA76 A 7 offset(5,37);
		MA76 A 1 offset(2,36);
		MA76 A 1 offset(0,34);
		goto readyend;

	user4:
	unload:
		---- A 1; //DO NOT set this frame to zero
		MA76 C 0{
			invoker.weaponstatus[0]|=MA76CF_UNLOADONLY;
			if(
				invoker.weaponstatus[MA76CS_MAG]>=0  
			){
				return resolvestate("unmag");
			}else if(
				invoker.weaponstatus[MA76CS_CHAMBER]>0  
			){
				return resolvestate("unloadchamber");
			}
			return resolvestate("nope");
		}
	reload:
		MA76 C 0{
			int inmag=invoker.weaponstatus[MA76CS_MAG];
			bool nomags=HDMagAmmo.NothingLoaded(self,"WAN_MA76CClip");
			bool haverounds=countinv("FourMilAmmo");
			invoker.weaponstatus[0]&=~MA76CF_UNLOADONLY;

			//no point reloading
			if(
						
				inmag>=52
				||(
					//no mags to load and can't directly load chamber
					nomags
					&&(
						!haverounds
						||inmag>=0
						||invoker.weaponstatus[MA76CS_CHAMBER]>0
								 
					)
				)
			)return resolvestate("nope");

			//no mag, empty chamber, have loose rounds
			if(
				inmag<0
				&&invoker.weaponstatus[MA76CS_CHAMBER]<1
				&&haverounds
				&&(
					pressinguse()
					||nomags
				)
			)return resolvestate("nope");
			else if(
				invoker.weaponstatus[MA76CS_MAG]>0  
			){
				//if full mag and unchambered, chamber
				if(
					invoker.weaponstatus[MA76CS_MAG]>=52 
					&&invoker.weaponstatus[MA76CS_CHAMBER]!=2
				){
					return resolvestate("chamber_manual");
				}				
			}
			return resolvestate("unmag");
		}

	unmag:
		MA76 C 1 offset(0,34);
		MA76 C 1 offset(2,36);
		MA76 D 1 offset(4,40);
		MA76 E 2 offset(8,42){
			A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
			A_StartSound("weapons/rifleclick2",8);
		}
		MA76 F 4 offset(14,46){
			A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
			A_StartSound ("weapons/rifleload",8,CHANF_OVERLAP);
		}
		MA76 F 0{
			int magamt=invoker.weaponstatus[MA76CS_MAG];
			if(magamt<0){setweaponstate("magout");return;}

			if(magamt>0){
				int fullets=clamp(52,0,magamt);
				magamt=fullets;
			}

			invoker.weaponstatus[MA76CS_MAG]=-1;
			if(
				!PressingReload()
				&&!PressingUnload()
			){
				HDMagAmmo.SpawnMag(self,"WAN_MA76CClip",magamt);
				setweaponstate("magout");
			}else{
				HDMagAmmo.GiveMag(self,"WAN_MA76CClip",magamt);
				setweaponstate("pocketmag");
			}
		}
	pocketmag:
		MA76 F 7 offset(12,52)A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		MA76 F 0 A_StartSound("weapons/pocket",9);
		MA76 FF 7 offset(14,54)A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		MA76 F 0{
		}goto magout;
	magout:
		MA76 F 4{
			invoker.weaponstatus[MA76CS_MAG]=-1;
			if(invoker.weaponstatus[0]&MA76CF_UNLOADONLY)setweaponstate("reloaddone");
		}goto loadmag;


	loadmag:
		MA76 F 0 A_StartSound("weapons/pocket",9);
		MA76 G 7 A_MuzzleClimb(frandom(-0.2,0.4),frandom(-0.2,0.8));
		MA76 G 9 A_StartSound("weapons/MA75B_reload1",8,CHANF_OVERLAP);
		MA76 H 8; //A_StartSound("weapons/MA75B_reload1",8,CHANF_OVERLAP);
		MA76 I 6 {
			let mmm=hdmagammo(findinventory("WAN_MA76CClip"));
			if(mmm){
				int minput=mmm.TakeMag(true);
				int rndcnt=minput;
				invoker.weaponstatus[MA76CS_MAG]=rndcnt;
				//A_StartSound("weapons/rifleclick",8);
				A_StartSound("weapons/MA75B_reload2",8,CHANF_OVERLAP);
			}
		}
		MA76 J 4; //A_StartSound("weapons/MA75B_reload2",8,CHANF_OVERLAP);
		goto reloaddone;

	reloaddone:
		MA76 K 3;
		MA76 L 2;
		goto chamber_manual;


	altfire:
		MA76 A 0 A_JumpIf(invoker.weaponstatus[MA76CS_GRENADES]>0,1);
		goto nope;
		MA76 A 2;
		MA76 A 3 A_Gunflash("firegrenade");
		MA76 A 5;
		goto nope;
	althold:
		MA76 A 0;
		goto nope;


	firegrenade:
		TNT1 A 0 A_JumpIf(invoker.weaponstatus[MA76CS_GRENADES]>0,1);
		stop;
		TNT1 A 1{
			invoker.airburst=0;
			A_Fire20mmGL();
			invoker.weaponstatus[MA76CS_GRENADES]-=1;
			A_StartSound("weapons/MA75B_grenade",CHAN_WEAPON);
			A_ZoomRecoil(0.95);
		}
		TNT1 A 2 A_MuzzleClimb(
			0,0,0,0,
			-0.6,-1.6,
			-0.2,-0.6
		);
		stop;

	altreload:
		MA76 A 0{
			int inpak=invoker.weaponstatus[MA76CS_GRENADES];
			bool nopaks=HDMagAmmo.NothingLoaded(self,"WAN_20mmGrenadePack");
			invoker.weaponstatus[0]&=~MA76CF_UNLOADONLY;

			//no point reloading
			if(	
				inpak>=7
				||(
					//no mags to load and can't directly load chamber
					nopaks
				)
			)return resolvestate("nope");
			return resolvestate("unpack");
		}
	unpack:
		MA76 A 1 offset(0,34);
		MA76 A 1 offset(2,36);
		MA76 A 1 offset(4,40);
		MA76 A 2 offset(8,42){
			A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
			A_StartSound("weapons/rifleclick2",8);
		}
		MA76 A 4 offset(14,46){
			A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
			A_StartSound ("weapons/grenopen",8,CHANF_OVERLAP);
		}
		MA76 A 0{
			int grenamt=invoker.weaponstatus[MA76CS_GRENADES];
			if(grenamt<0){setweaponstate("packout");return;}

			invoker.weaponstatus[MA76CS_GRENADES]=-1;
			if(
				!PressingAltReload()
				&&!PressingUnload()
			){
				HDMagAmmo.SpawnMag(self,"WAN_20mmGrenadePack",grenamt);
				setweaponstate("packout");
			}else{
				HDMagAmmo.GiveMag(self,"WAN_20mmGrenadePack",grenamt);
				setweaponstate("pocketpack");
			}
		}
	pocketpack:
		MA76 A 7 offset(12,52)A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		MA76 A 0 A_StartSound("weapons/pocket",9);
		MA76 AA 7 offset(14,54)A_MuzzleClimb(frandom(-0.2,0.8),frandom(-0.2,0.4));
		MA76 A 0{
		}goto packout;
	packout:
		MA76 A 4{
			invoker.weaponstatus[MA76CS_GRENADES]=-1;
			if(invoker.weaponstatus[0]&MA76CF_UNLOADONLY)setweaponstate("altreloaddone");
		}goto loadpack;	
	loadpack:
		MA76 A 0 A_StartSound("weapons/pocket",9);
		MA76 AA 6 offset(14,54)A_MuzzleClimb(frandom(-0.2,0.4),frandom(-0.2,0.8));
		MA76 A 2 offset(13,52){
			let mmm=hdmagammo(findinventory("WAN_20mmGrenadePack"));
			if(mmm){
				int minput=mmm.TakeMag(true);
				int rndcnt=minput;
				invoker.weaponstatus[MA76CS_GRENADES]=rndcnt;
				//A_StartSound("weapons/rifleclick",8);
				A_StartSound("weapons/grenreload",8,CHANF_OVERLAP);
			}
		}
		MA76 A 2 offset(13,54);
		MA76 A 3 offset(14,57);
		MA76 A 2 offset(14,55);
		MA76 A 15 offset(14,54);
		MA76 A 2 offset(12,52);
		MA76 A 2 offset(10,50);
		MA76 A 2 offset(8,46) A_StartSound("weapons/MA75B_click",8,CHANF_OVERLAP);
		MA76 A 1 offset(10,44);
		MA76 A 2 offset(13,42);
		MA76 A 2 offset(11,43);
		MA76 A 3 offset(8,45);
		goto altreloaddone;

	altreloaddone:
		MA76 A 1 offset (4,40);
		MA76 A 1 offset (2,34);
		goto ready;

	spawn:
		M76C AB -1 nodelay{

			if(invoker.weaponstatus[MA76CS_MAG]<0)frame=1;
			else frame=0;

		}
		M76C AB -1;
		stop;
	}
	override void InitializeWepStats(bool idfa){
		weaponstatus[MA76CS_MAG]=52;
		weaponstatus[MA76CS_GRENADES]=7;
		weaponstatus[MA76CS_CHAMBER]=2;
		if(!idfa&&!owner){
			weaponstatus[MA76CS_HEAT]=0;
		}
	}
	override void loadoutconfigure(string input){

		int firemode=getloadoutvar(input,"firemode",1);
		if(firemode>0)weaponstatus[0]&=~MA76CF_FULLAUTO; //|=MA76CF_FULLAUTO;
		else weaponstatus[0]|=MA76CF_FULLAUTO; //&=~MA76CF_FULLAUTO;

		int semi=getloadoutvar(input,"semi",1);
		if(semi>0){
			weaponstatus[0]|=MA76CF_NOAUTO;
			weaponstatus[0]&=~MA76CF_FULLAUTO;
		}else weaponstatus[0]&=~MA76CF_NOAUTO;
		
		int nopack=getloadoutvar(input,"nopack",1);
		if(nopack>0)weaponstatus[MA76CS_GRENADES]=-1;
		else weaponstatus[MA76CS_GRENADES]=7;
		
		int scope=getloadoutvar(input,"scope",1);
		if(scope>0)weaponstatus[0]|=MA76CF_SCOPE;
		else weaponstatus[0]&=~MA76CF_SCOPE;
		
		int xhdot=getloadoutvar(input,"dot",3);
		int reflex=getloadoutvar(input,"reflex",1);
		if(
			!reflex
			&&xhdot<0
		)weaponstatus[0]&=~MA76CF_DTSIT;
		else{
			weaponstatus[0]|=MA76CF_DTSIT;
			if(xhdot>=0)weaponstatus[MA76CS_DOT]=xhdot;
		}
	}
}
enum ma76cstatus{
	MA76CF_FULLAUTO=1,
	MA76CF_SCOPE=2,
	MA76CF_UNLOADONLY=4,
	MA76CF_NOAUTO=8,
	MA76CF_DTSIT=16,

	MA76CS_FLAGS=0,
	MA76CS_CHAMBER=1,
	MA76CS_MAG=2, //-1 is empty
	MA76CS_HEAT=3,
	MA76CS_DOT=4,
	MA76CS_GRENADES=7,
};

class WAN_MA76CClip:HDMagAmmo{
	default{
		//$Category "Ammo/Hideous Destructor/"
		//$Title "4.26mm UAC Standard Mag"
		//$Sprite "CLIPB0"

		hdmagammo.maxperunit 52;
		hdmagammo.roundtype "FourMilAmmo";
		hdmagammo.roundbulk ENC_426_LOADED;
		hdmagammo.magbulk ENC_426MAG_EMPTY*1.2;
		hdmagammo.extracttime 8;
		tag "MA-76C clip";
		hdpickup.refid '452';
		//inventory.pickupmessage "Picked up a MA-76C clip.";
		scale 0.5;
	}
	
	override string pickupmessage() {return Stringtable.Localize("$WAN_PICK_MA76CCLIP");}
	
	override void GetItemsThatUseThis(){
		itemsthatusethis.push("WAN_MA76C");
	}
	override void postbeginplay(){
		super.postbeginplay();
		sealtimer=0;
		breakchance=0;
		
	}
	int breakchance;
	int sealtimer;
	override void doeffect(){
		if(sealtimer>0)sealtimer--;
		if(breakchance>0)breakchance--;
		super.doeffect();
	}
	override string,string,name,double getmagsprite(int thismagamt){
		string magsprite;
		if(thismagamt<1)magsprite="M76MB0";
		else magsprite="M76MA0";
		return magsprite,"RBRSBRN","FourMilAmmo",1.2;
	}
		override bool Extract(){
		SyncAmount();
		int mindex=mags.size()-1;
		if(
			mags.size()<1
			||mags[mindex]<1
			||owner.A_JumpIfInventory(roundtype,0,"null")
		)return false;
		extracttime=getdefaultbytype(getclass()).extracttime;
		int totake=min(random(1,24),mags[mindex]);
		if(totake<HDPickup.MaxGive(owner,roundtype,roundbulk))HDF.Give(owner,roundtype,totake);
		else HDPickup.DropItem(owner,roundtype,totake);
		owner.A_StartSound("weapons/rifleclick2",CHAN_WEAPON);
		owner.A_StartSound("weapons/rockreload",CHAN_WEAPON,CHANF_OVERLAP,0.4);
		mags[mindex]-=totake;
		return true;
	}
	override bool Insert(){
		SyncAmount();
		if(
			mags.size()<1
			||mags[mags.size()-1]>=maxperunit
			||!owner.countinv(roundtype)
		)return false;
		owner.A_TakeInventory(roundtype,1,TIF_NOTAKEINFINITE);
		owner.A_StartSound("weapons/rifleclick2",8);
		if(random(0,80)<=breakchance){
			owner.A_StartSound("weapons/bigcrack",8,CHANF_OVERLAP);
			owner.A_SpawnItemEx("WallChunk",12,0,owner.height-12,4,frandom(-2,2),frandom(2,4));
			breakchance=min(breakchance+15,80);
			return false;
		}
		breakchance=max(breakchance,15);
		owner.A_StartSound("weapons/pocket",9,volume:frandom(0.1,0.6));
		mags[mags.size()-1]++;
		return true;
	}
	override void Consolidate(){
		SyncAmount();
		if(amount<2)return;
		int totalrounds=0;
		int howmanymags=0;
		for(int i=0;i<amount;i++){
			int thismag=mags[i];
			if(
				!thismag
				||thismag>=52
			)continue;
			howmanymags++;
			totalrounds+=mags[i]%52;
			mags[i]=0; //keep the empties, do NOT call clear()!
		}
		if(howmanymags>1)totalrounds=int(totalrounds*frandom(0.9,1.));
		for(int i=0;i<amount;i++){
			if(mags[i]>=52)continue;
			int toinsert=clamp(totalrounds,mags[i],52);
			mags[i]=toinsert;
			totalrounds-=toinsert;
			if(totalrounds<1)break;
		}
	}
	states(actor){
	spawn:
		M76M AB -1 nodelay{
			int mmm=mags[0];
		}stop;
	spawnempty:
		M76M B -1{
			brollsprite=true;brollcenter=true;
			roll=randompick(0,0,0,0,2,2,2,2,1,3)*90;
		}stop;
	}
}

class WAN_20mmGrenadePack:HDMagAmmo{
	default{
		hdmagammo.maxperunit 7;
		hdmagammo.roundtype "WAN_20mmGrenadeAmmo";
		hdmagammo.roundbulk ENC_20MMGRENADELOADED;
		hdmagammo.magbulk 8;
		hdmagammo.inserttime 10;
		hdmagammo.extracttime 8;
		tag "$WAN_TAG_MA76CPACK";
		//inventory.pickupmessage "Picked up a MA-76C grenade pack.";
		hdpickup.refid '2g7';
		scale 0.4;
	}
	
	override string pickupmessage() {return Stringtable.Localize("$WAN_PICK_MA76CPACK");}
	
	override string,string,name,double getmagsprite(int thismagamt){
		string magsprite;
		switch (thismagamt)
		{
			case 0:
				magsprite="M76GA0";
				break;
			case 1:
				magsprite="M76GB0";
				break;
			case 2:
				magsprite="M76GC0";
				break;
			case 3:
				magsprite="M76GD0";
				break;
			case 4:
				magsprite="M76GE0";
				break;
			case 5:
				magsprite="M76GF0";
				break;
			case 6:
				magsprite="M76GG0";
				break;
			case 7:
				magsprite="M76GH0";
				break;
			default:
				magsprite="M76GA0";
				break;
		}
		return magsprite,"MA7GB0","WAN_20mmGrenadeAmmo",0.5;
	}
	
	override void GetItemsThatUseThis(){
		itemsthatusethis.push("WAN_MA76C");
	}
	
	states(actor){
	spawn:
		M76G ABCDEFGH -1 nodelay{
			if(!mags.size()){destroy();return;}
			int amt=mags[0];
			frame=amt;
		}stop;
	spawnempty:
		M76G A -1{
			//brollsprite=true;brollcenter=true;
			//roll=randompick(0,0,0,0,2,2,2,2,1,3)*90;
		}stop;
	}
}

class WAN_MA76CSpawner:IdleDummy{
	states{
	spawn:
		TNT1 A 0 nodelay{
			let www=WAN_MA76C(spawn("WAN_MA76C",pos,ALLOW_REPLACE));
			if(!www)return;
			HDF.TransferSpecials(self, www);
			if(!random(0,5))www.weaponstatus[0]|=MA76CF_SCOPE;
			if(!random(0,2))www.weaponstatus[0]|=MA76CF_DTSIT;

			spawn("WAN_20mmGrenadePack",pos+(10,0,0),ALLOW_REPLACE);
			spawn("WAN_MA76CClip",pos+(7,0,0),ALLOW_REPLACE);
			spawn("WAN_MA76CClip",pos+(5,0,0),ALLOW_REPLACE);
		}stop;
	}
}
