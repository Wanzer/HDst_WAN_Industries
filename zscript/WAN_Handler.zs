// using Peppergrinder's event handler code for the time being, i just *really* want be able to disable specific item spawns for now

class WanInjectorData
{
	class<Actor> Item;
	class<Actor> Spawner;
	int Chance;

	static WanInjectorData Create(class<Actor> item, class<Actor> spawner, int ch)
	{
		WanInjectorData Data = new("WanInjectorData");
		Data.Item = item;
		Data.Spawner = spawner;
		Data.Chance = ch;
		return Data;
	}
}

class WanInjector : StaticEventHandler
{
	enum WeaponSpawnFlags1
	{
		WSF_WSTEM5			= 1 << 0,
		WSF_MA75B			= 1 << 1,
		WSF_MA76C			= 1 << 2,
		WSF_SPNKR			= 1 << 3,
		//WSF_MAUG03			= 1 << 4,
	}

	enum AmmoSpawnFlags1
	{
		ASF_MA75BMags		= 1 << 0,
		ASF_MA75BPack		= 1 << 1,
		ASF_MA76CMags		= 1 << 2,
		ASF_MA76CPack		= 1 << 3,
		ASF_SPNKRRockets	= 1 << 4
	}
	
	// [Ace] This is only used for the Enable All option. Remember to change it.
	const WeaponCount = 4;
	const AmmoCount = 5;

	private Array<WanInjectorData> SpawnData;

	override void WorldLoaded(WorldEvent e)
	{

		SpawnData.Clear();

		int WeaponSpawns1 = wan_weaponspawns_1;
		int AmmoSpawns1 = wan_ammospawns_1;

		// ----- WEAPONS -----

		if (WeaponSpawns1 & WSF_WSTEM5)		{ SpawnData.Push(WanInjectorData.Create("WAN_WSTEM5Spawner",		"SSGReplaces",			48)); }
		if (WeaponSpawns1 & WSF_MA75B)		{ SpawnData.Push(WanInjectorData.Create("WAN_MA75BSpawner",		"PlasmaReplaces",		48)); }
		if (WeaponSpawns1 & WSF_MA76C)		{ SpawnData.Push(WanInjectorData.Create("WAN_MA76CSpawner",		"ChaingunReplaces",		48)); }
		if (WeaponSpawns1 & WSF_SPNKR)		{ SpawnData.Push(WanInjectorData.Create("WAN_SPNKR",				"RLReplaces",			48)); }

		// ----- AMMO -----
		if (AmmoSpawns1 & ASF_MA75BMags)
		{
			SpawnData.Push(WanInjectorData.Create("WAN_MA75BClip", "CellRandom", 16));
		}

		if (AmmoSpawns1 & ASF_MA75BPack)
		{
			SpawnData.Push(WanInjectorData.Create("WAN_GrenadePackSpawner", "RocketBoxRandom", 8));
		}

		if (AmmoSpawns1 & ASF_MA76CMags)
		{
			SpawnData.Push(WanInjectorData.Create("WAN_MA76CClip", "ClipMagPickup", 16));
		}

		if (AmmoSpawns1 & ASF_MA76CPack)
		{
			SpawnData.Push(WanInjectorData.Create("WAN_20mmGrenadePack", "HDRocketAmmo", 32));
		}

		if (AmmoSpawns1 & ASF_SPNKRRockets)
		{
			SpawnData.Push(WanInjectorData.Create("WAN_CustomRocketPickup", "RocketBoxRandom", 24));
		}
	}
	
	
	override void CheckReplacement(ReplaceEvent e)
	{
		if (!e.Replacement)
		{
			return;
		}

		for (int i = 0; i < SpawnData.Size(); ++i)
		{
			if (e.Replacement == SpawnData[i].Spawner && random[saltrand]() <= SpawnData[i].Chance)
			{
				e.Replacement = SpawnData[i].Item;
			}
		}
	}

	override void WorldThingSpawned(WorldEvent e)
	{
		let WanAmmo = HDAmmo(e.Thing);
		if (WanAmmo)
		{
			switch (WanAmmo.GetClassName())
			{
				case 'HDShellAmmo'://12 gauge
					//WanAmmo.ItemsThatUseThis.Push("Wan_WSTEM5"); this one's handled by the gun
					break;
				case 'FourMilAmmo'://4.26
					WanAmmo.ItemsThatUseThis.Push("Wan_MA76C");
					break;
				case 'SevenMilAmmo'://7.76
					WanAmmo.ItemsThatUseThis.Push("Wan_MA75B");
					break;
				case 'HDRocketAmmo':
					WanAmmo.ItemsThatUseThis.Push("Wan_MA75B");
					break;
				case 'HeatAmmo':
					WanAmmo.ItemsThatUseThis.Push("Wan_SPNKR");
					break;
				case 'WAN_20mmGrenadeAmmo':
					WanAmmo.ItemsThatUseThis.Push("Wan_MA76C");
					break;
				case 'Wan_ThuRKTAmmo':
					WanAmmo.ItemsThatUseThis.Push("Wan_SPNKR");
					break;
				case 'Wan_TortRKTAmmo':
					WanAmmo.ItemsThatUseThis.Push("Wan_SPNKR");
					break;
			}
		}
	}
	
	override void NetworkProcess(ConsoleEvent e)
	{
		if (e.Player != consoleplayer)
		{
			return;
		}

		// [Ace] In case you add another CVar, uncomment the respective lines.
		if (e.Name ~== "WAN_EnableAll_Weapons")
		{
			int CurrFlags[1]; // [Ace] Number here is the number of CVars.

			for (int i = 0; i < WeaponCount; ++i)
			{
				CurrFlags[i / 32] |= (1 << (i % 32));
			}

			CVar.GetCVar("wan_weaponspawns_1").SetInt(CurrFlags[0]);
			//CVar.GetCVar("pepper_weaponspawns_2").SetInt(CurrFlags[1]);
		}

		if (e.Name ~== "WAN_EnableAll_Ammo")
		{
			int CurrFlags = wan_ammospawns_1;
			for (int i = 0; i < AmmoCount; ++i)
			{
				CurrFlags |= (1 << i);
			}
			CVar.GetCVar("wan_ammospawns_1").SetInt(CurrFlags);
		}
	}
}
